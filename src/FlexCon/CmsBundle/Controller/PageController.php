<?php
// src/FlexCon/CmsBundle/Controller/PageController.php

namespace FlexCon\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// Import new namespaces
use FlexCon\CmsBundle\Entity\Enquiry;
use FlexCon\CmsBundle\Form\EnquiryType;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
#    public function indexAction()
#    {
#        return $this->render('FlexConCmsBundle:Page:index.html.twig');
#    }

    public function indexAction()
    {
        $em = $this->getDoctrine()
                   ->getManager();

        $blogs = $em->createQueryBuilder()
                    ->select('b')
                    ->from('FlexConCmsBundle:Blog',  'b')
                    ->addOrderBy('b.created', 'DESC')
                    ->getQuery()
                    ->getResult();

        return $this->render('FlexConCmsBundle:Page:index.html.twig', array(
            'blogs' => $blogs
        ));
    }

    public function aboutAction()
    {
        return $this->render('FlexConCmsBundle:Page:about.html.twig');
    }

#    public function contactAction()
#    {
#        return $this->render('FlexConCmsBundle:Page:contact.html.twig');
#    }

	public function contactAction(Request $request)
	{
		$enquiry = new Enquiry();
		$form = $this->createForm(EnquiryType::class, $enquiry);

// RMd This: after adding whateverAction(Request $request) \\
// AND inserting:  use Symfony\Component\HttpFoundation\Request;
//		$request = $this->getRequest();
// Added This:
        $this->request = $request;
			if ($request->getMethod() == 'POST') {
			$form->bind($request);

			if ($form->isValid()) {
				// Perform some action, such as sending an email
				
				$message = \Swift_Message::newInstance()
					->setSubject('Contact enquiry from FlexCon')
					->setFrom('enquiries@voyatrax.org')
					->setTo($this->container->getParameter('flex_com_cms.emails.contact_email'))
					->setBody($this->renderView('FlexConCmsBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));
				$this->get('mailer')->send($message);
			
			$this->get('session')->setFlashbag('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');

        
				// Redirect - This is important to prevent users re-posting
				// the form if they refresh the page
				return $this->redirect($this->generateUrl('flex_com_cms_contact'));
			}
		}

		return $this->render('FlexConCmsBundle:Page:contact.html.twig', array(
			'form' => $form->createView()
		));
	}


    public function termsAction()
    {
        return $this->render('FlexConCmsBundle:Page:terms.html.twig');
    }

    public function policyAction()
    {
        return $this->render('FlexConCmsBundle:Page:policy.html.twig');
    }

    public function licenseAction()
    {
        return $this->render('FlexConCmsBundle:Page:license.html.twig');
    }

    public function imprintAction()
    {
        return $this->render('FlexConCmsBundle:Page:imprint.html.twig');
    }

public function sidebarAction()
{
    $em = $this->getDoctrine()
               ->getManager();

    $tags = $em->getRepository('FlexConCmsBundle:Blog')
               ->getTags();

    $tagWeights = $em->getRepository('FlexConCmsBundle:Blog')
                     ->getTagWeights($tags);

    $commentLimit   = $this->container
                           ->getParameter('flex_con_cms.comments.latest_comment_limit');
    $latestComments = $em->getRepository('FlexConCmsBundle:Comment')
                         ->getLatestComments($commentLimit);

    return $this->render('FlexConCmsBundle:Page:sidebar.html.twig', array(
        'latestComments'    => $latestComments,
        'tags'              => $tagWeights
    ));
}



}
