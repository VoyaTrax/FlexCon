<?php
// src/FlexCon/CmsBundle/Controller/BlogController.php

namespace FlexCon\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Blog controller.
 */
class BlogController extends Controller
{
    /**
     * Show a blog entry
     */
    public function showAction($id, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('FlexConCmsBundle:Blog')->find($id);

        if (!$blog) {
        throw $this->createNotFoundException('Unable to find Blog post.');
    }

    $comments = $em->getRepository('FlexConCmsBundle:Comment')
                   ->getCommentsForBlog($blog->getId());

    return $this->render('FlexConCmsBundle:Blog:show.html.twig', array(
        'blog'      => $blog,
        'comments'  => $comments
        ));
    }
}
