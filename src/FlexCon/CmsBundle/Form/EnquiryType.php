<?php
// src/FlexCon/CmsBundle/Form/EnquiryType.php

namespace FlexCon\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
// Add these---
#use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnquiryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('email');
        $builder->add('subject');
        $builder->add('body');
    }

    public function getName()
    {
        return 'contact';
    }
}
