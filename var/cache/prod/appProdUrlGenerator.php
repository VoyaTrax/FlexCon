<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'flex_con_cms_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::indexAction',  ),  2 =>   array (    '_methods' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_about' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::aboutAction',  ),  2 =>   array (    '_methods' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/about',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_contact' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::contactAction',  ),  2 =>   array (    '_methods' => 'GET|POST',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/contact',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_terms' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::termsAction',  ),  2 =>   array (    '_methods' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/terms',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_policy' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::policyAction',  ),  2 =>   array (    '_methods' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/policy',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_license' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::licenseAction',  ),  2 =>   array (    '_methods' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/license',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_imprint' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::imprintAction',  ),  2 =>   array (    '_methods' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/imprint',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_blog_show' => array (  0 =>   array (    0 => 'id',    1 => 'slug',  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\BlogController::showAction',  ),  2 =>   array (    '_methods' => 'GET',    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'slug',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'flex_con_cms_comment_create' => array (  0 =>   array (    0 => 'blog_id',  ),  1 =>   array (    '_controller' => 'FlexCon\\CmsBundle\\Controller\\CommentController::createAction',  ),  2 =>   array (    '_methods' => 'POST',    'blog_id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'blog_id',    ),    1 =>     array (      0 => 'text',      1 => '/comment',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
