<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // flex_con_cms_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'flex_con_cms_homepage');
            }

            return array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::indexAction',  '_route' => 'flex_con_cms_homepage',);
        }

        // flex_con_cms_about
        if ($pathinfo === '/about') {
            return array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::aboutAction',  '_route' => 'flex_con_cms_about',);
        }

        // flex_con_cms_contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::contactAction',  '_route' => 'flex_con_cms_contact',);
        }

        // flex_con_cms_terms
        if ($pathinfo === '/terms') {
            return array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::termsAction',  '_route' => 'flex_con_cms_terms',);
        }

        // flex_con_cms_policy
        if ($pathinfo === '/policy') {
            return array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::policyAction',  '_route' => 'flex_con_cms_policy',);
        }

        // flex_con_cms_license
        if ($pathinfo === '/license') {
            return array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::licenseAction',  '_route' => 'flex_con_cms_license',);
        }

        // flex_con_cms_imprint
        if ($pathinfo === '/imprint') {
            return array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\PageController::imprintAction',  '_route' => 'flex_con_cms_imprint',);
        }

        // flex_con_cms_blog_show
        if (preg_match('#^/(?P<id>\\d+)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'flex_con_cms_blog_show')), array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\BlogController::showAction',));
        }

        // flex_con_cms_comment_create
        if (0 === strpos($pathinfo, '/comment') && preg_match('#^/comment/(?P<blog_id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'flex_con_cms_comment_create')), array (  '_controller' => 'FlexCon\\CmsBundle\\Controller\\CommentController::createAction',));
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
