<?php

/* FlexConCmsBundle:Page:terms.html.twig */
class __TwigTemplate_ed8a8ef842fe8a61bec165398373d6b94fe5cce326aecd732b5a31372f53ef45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:terms.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Terms";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "    <header>
        <h1>Terms of Use</h1>
    </header>
    <article>
        <p>Some sort of terms can be hardcoded here, we will added this to the backend at some point.</p>
    </article>
";
    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:terms.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  35 => 7,  29 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/terms.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Terms{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Terms of Use</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Some sort of terms can be hardcoded here, we will added this to the backend at some point.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
