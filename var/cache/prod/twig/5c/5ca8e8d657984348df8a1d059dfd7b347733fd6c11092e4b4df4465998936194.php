<?php

/* FlexConCmsBundle:Page:policy.html.twig */
class __TwigTemplate_4f57329df44aa847edee52b4de4d5c3bab1d1aec821e6fc6f3c5a8f1e7f1b207 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:policy.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Privacy Policy";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "    <header>
        <h1>Privacy Policy</h1>
    </header>
    <article>
        <p>Privacy Policy info coming soon.</p>
    </article>
";
    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:policy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  35 => 7,  29 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/policy.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Privacy Policy{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Privacy Policy</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Privacy Policy info coming soon.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
