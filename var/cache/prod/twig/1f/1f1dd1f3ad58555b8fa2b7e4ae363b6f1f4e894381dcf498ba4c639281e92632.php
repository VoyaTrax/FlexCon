<?php

/* FlexConCmsBundle::layout.html.twig */
class __TwigTemplate_15a3cf85eb8925c5af044d7ecd1b41e03545fb29a575626250cf44a3d03bf7db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "FlexConCmsBundle::layout.html.twig", 2);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/flexconcms/css/blog.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
    
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/flexconcms/css/sidebar.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
    }

    // line 13
    public function block_sidebar($context, array $blocks = array())
    {
        // line 14
        echo "   ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FlexConCmsBundle:Page:sidebar"));
        echo "
";
    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 14,  48 => 13,  42 => 8,  37 => 6,  32 => 5,  29 => 4,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/layout.html.twig #}*/
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('bundles/flexconcms/css/blog.css') }}" type="text/css" rel="stylesheet" />*/
/*     */
/*     <link href="{{ asset('bundles/flexconcms/css/sidebar.css') }}" type="text/css" rel="stylesheet" />*/
/* {% endblock %}*/
/* */
/* */
/* */
/* {% block sidebar %}*/
/*    {{ render(controller( 'FlexConCmsBundle:Page:sidebar' )) }}*/
/* {% endblock %}*/
/* */
/* */
