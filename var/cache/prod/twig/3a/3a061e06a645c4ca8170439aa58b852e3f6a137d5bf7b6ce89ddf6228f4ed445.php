<?php

/* FlexConCmsBundle:Comment:form.html.twig */
class __TwigTemplate_1ab044f77664f0e6fd81110de477dbb7246e2f47ee49a986668c5e170732be21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<form action=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("flex_con_cms_comment_create", array("blog_id" => $this->getAttribute($this->getAttribute((isset($context["comment"]) ? $context["comment"] : null), "blog", array()), "id", array()))), "html", null, true);
        echo "\" method=\"post\" class=\"blogger\">";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "
    <p>
        <input type=\"submit\" value=\"Submit\">
    </p>
</form>
";
    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Comment:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 4,  22 => 3,  19 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Comment/form.html.twig #}*/
/* */
/* <form action="{{ path('flex_con_cms_comment_create', { 'blog_id' : comment.blog.id } ) }}" method="post" class="blogger">{{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     <p>*/
/*         <input type="submit" value="Submit">*/
/*     </p>*/
/* </form>*/
/* */
