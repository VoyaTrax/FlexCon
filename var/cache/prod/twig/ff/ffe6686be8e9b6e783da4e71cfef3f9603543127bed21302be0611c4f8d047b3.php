<?php

/* FlexConCmsBundle:Page:imprint.html.twig */
class __TwigTemplate_eb50ef01ee00d6539b57435795629cf68d72e71bd65931add7ca73990a767440 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:imprint.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Imprint";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "    <header>
        <h1>Imprint</h1>
    </header>
    <article>
        <p>     
\t\t<h3>Site Owner</h3>
\t\t\tJohn Example<br>
\t\t\tExample Street. 1<br>
\t\t\t11111 Example City<br>
\t\t\tCountry<br>
\t\t<br><br>
\t\t<h3>Contact</h3>
\t\t\tTelephone: +00 666 666 666 66<br>
\t\t\tFax: +00 666 666 666 66<br>
\t\t<br>
\t\t\tE-Mail: <a href=\"mailto:john@example.com\">john@example.com</a><br>
\t\t<br>
\t\t\tWWW: <a href=\"http://www.example.com\" target=\"_blank\">www.example,com</a><br>
\t\t<br><br>
\t\t<h3>Content responsiblity</h3>
\t\t\tJohn Example<br>
\t\t\tExample Street. 1<br>
\t\t\t11111 Example City<br>
\t\t\tCountry<br>
\t\t<br><br>
\t\t<h3>Images and graphics source</h3>
\t\t\t<a href=\"http://www.example.com\" target=\"_blank\">www.example.com</a><br><br>
\t\t</p>
    </article>
";
    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:imprint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  35 => 7,  29 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/imprint.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Imprint{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Imprint</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>     */
/* 		<h3>Site Owner</h3>*/
/* 			John Example<br>*/
/* 			Example Street. 1<br>*/
/* 			11111 Example City<br>*/
/* 			Country<br>*/
/* 		<br><br>*/
/* 		<h3>Contact</h3>*/
/* 			Telephone: +00 666 666 666 66<br>*/
/* 			Fax: +00 666 666 666 66<br>*/
/* 		<br>*/
/* 			E-Mail: <a href="mailto:john@example.com">john@example.com</a><br>*/
/* 		<br>*/
/* 			WWW: <a href="http://www.example.com" target="_blank">www.example,com</a><br>*/
/* 		<br><br>*/
/* 		<h3>Content responsiblity</h3>*/
/* 			John Example<br>*/
/* 			Example Street. 1<br>*/
/* 			11111 Example City<br>*/
/* 			Country<br>*/
/* 		<br><br>*/
/* 		<h3>Images and graphics source</h3>*/
/* 			<a href="http://www.example.com" target="_blank">www.example.com</a><br><br>*/
/* 		</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
