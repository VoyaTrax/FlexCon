<?php

/* FlexConCmsBundle:Page:license.html.twig */
class __TwigTemplate_581e9d4019faed11749ea899545d09880e8af308ecc8049ca77469d73eadb6d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:license.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "License";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "\t<header>
\t\t<h1>Licenses used</h1>
\t</header>
    <article>
\t\t<p><b>NOTE:</b> The following applies unless otherwise noted.</p>
\t\t<p>
\t\t\t<h2>Software:</h2>
\t\t\t\tThe MIT License (MIT)<br>
\t\t\t<br>
\t\t\t\tCopyright (c) 2014-";
        // line 16
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " JMColeman<br>
\t\t\t<br>
\t\t\t\tPermission is hereby granted, free of charge, to any person obtaining a copy<br>
\t\t\t\tof this software and associated documentation files (the \"Software\"), to deal<br>
\t\t\t\tin the Software without restriction, including without limitation the rights<br>
\t\t\t\tto use, copy, modify, merge, publish, distribute, sublicense, and/or sell<br>
\t\t\t\tcopies of the Software, and to permit persons to whom the Software is<br>
\t\t\t\tfurnished to do so, subject to the following conditions:<br>
\t\t\t<br>
\t\t\t\tThe above copyright notice and this permission notice shall be included in<br>
\t\t\t\tall copies or substantial portions of the Software.<br>
\t\t\t<br>
\t\t\t\tTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR<br>
\t\t\t\tIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,<br>
\t\t\t\tFITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE<br>
\t\t\t\tAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER<br>
\t\t\t\tLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,<br>
\t\t\t\tOUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN<br>
\t\t\t\tTHE SOFTWARE.
\t\t</p>
\t\t<p>
\t\t\t<h2>Documentation:</h2>
\t\t\t\t<a rel=\"license\" href=\"http://creativecommons.org/licenses/by/3.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by/3.0/88x31.png\" /></a><br />Documentation is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by/3.0/\">Creative Commons Attribution 3.0 Unported License</a>.
\t\t</p>
\t\t<p>
\t\t\t<h2>Copyrighted:</h2>
\t\t\t\tLogos, Tradmarks, images, etc., that represent the VoyaTrax Project are to be considered copyright Jimmy M. Coleman and/or the Project itself.<br>For details contact the <a href=\"mailto://license@voyatrax.org\">license department</a>.
\t\t</p>
    </article>
";
    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:license.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 16,  38 => 7,  35 => 6,  29 => 4,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/license.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* {% block title %}License{% endblock%}*/
/* */
/* {% block body %}*/
/* 	<header>*/
/* 		<h1>Licenses used</h1>*/
/* 	</header>*/
/*     <article>*/
/* 		<p><b>NOTE:</b> The following applies unless otherwise noted.</p>*/
/* 		<p>*/
/* 			<h2>Software:</h2>*/
/* 				The MIT License (MIT)<br>*/
/* 			<br>*/
/* 				Copyright (c) 2014-{{ "now"|date ("Y") }} JMColeman<br>*/
/* 			<br>*/
/* 				Permission is hereby granted, free of charge, to any person obtaining a copy<br>*/
/* 				of this software and associated documentation files (the "Software"), to deal<br>*/
/* 				in the Software without restriction, including without limitation the rights<br>*/
/* 				to use, copy, modify, merge, publish, distribute, sublicense, and/or sell<br>*/
/* 				copies of the Software, and to permit persons to whom the Software is<br>*/
/* 				furnished to do so, subject to the following conditions:<br>*/
/* 			<br>*/
/* 				The above copyright notice and this permission notice shall be included in<br>*/
/* 				all copies or substantial portions of the Software.<br>*/
/* 			<br>*/
/* 				THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR<br>*/
/* 				IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,<br>*/
/* 				FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE<br>*/
/* 				AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER<br>*/
/* 				LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,<br>*/
/* 				OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN<br>*/
/* 				THE SOFTWARE.*/
/* 		</p>*/
/* 		<p>*/
/* 			<h2>Documentation:</h2>*/
/* 				<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br />Documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.*/
/* 		</p>*/
/* 		<p>*/
/* 			<h2>Copyrighted:</h2>*/
/* 				Logos, Tradmarks, images, etc., that represent the VoyaTrax Project are to be considered copyright Jimmy M. Coleman and/or the Project itself.<br>For details contact the <a href="mailto://license@voyatrax.org">license department</a>.*/
/* 		</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
