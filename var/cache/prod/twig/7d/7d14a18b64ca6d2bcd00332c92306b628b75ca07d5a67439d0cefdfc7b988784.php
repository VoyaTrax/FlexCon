<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_10ce4ebd8d5d5609a75e59dfd3c5fe0fcbfb81ba7427c423cbf77d5b6e6c8896 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
