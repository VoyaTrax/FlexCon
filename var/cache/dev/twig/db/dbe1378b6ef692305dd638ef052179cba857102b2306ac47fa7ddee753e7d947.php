<?php

/* FlexConCmsBundle:Page:terms.html.twig */
class __TwigTemplate_d254a8de52aa2d467a8d10442e8362e772e6a32cc5d329b263206f6d9f1db04f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:terms.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8bbbd36380fa72a2f76f9db1f990eccb0765290a1a491d98bcbac835f0395d9f = $this->env->getExtension("native_profiler");
        $__internal_8bbbd36380fa72a2f76f9db1f990eccb0765290a1a491d98bcbac835f0395d9f->enter($__internal_8bbbd36380fa72a2f76f9db1f990eccb0765290a1a491d98bcbac835f0395d9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:terms.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8bbbd36380fa72a2f76f9db1f990eccb0765290a1a491d98bcbac835f0395d9f->leave($__internal_8bbbd36380fa72a2f76f9db1f990eccb0765290a1a491d98bcbac835f0395d9f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_93298b8e1f04b65febde09a4e80b9cae7f770cad11559e6488a991ac71e9cf48 = $this->env->getExtension("native_profiler");
        $__internal_93298b8e1f04b65febde09a4e80b9cae7f770cad11559e6488a991ac71e9cf48->enter($__internal_93298b8e1f04b65febde09a4e80b9cae7f770cad11559e6488a991ac71e9cf48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Terms";
        
        $__internal_93298b8e1f04b65febde09a4e80b9cae7f770cad11559e6488a991ac71e9cf48->leave($__internal_93298b8e1f04b65febde09a4e80b9cae7f770cad11559e6488a991ac71e9cf48_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_116eda700d9898cca6ba4764ad233d44972b08ab0f8e2546082e79294f27ca00 = $this->env->getExtension("native_profiler");
        $__internal_116eda700d9898cca6ba4764ad233d44972b08ab0f8e2546082e79294f27ca00->enter($__internal_116eda700d9898cca6ba4764ad233d44972b08ab0f8e2546082e79294f27ca00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Terms of Use</h1>
    </header>
    <article>
        <p>Some sort of terms can be hardcoded here, we will added this to the backend at some point.</p>
    </article>
";
        
        $__internal_116eda700d9898cca6ba4764ad233d44972b08ab0f8e2546082e79294f27ca00->leave($__internal_116eda700d9898cca6ba4764ad233d44972b08ab0f8e2546082e79294f27ca00_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:terms.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/terms.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Terms{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Terms of Use</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Some sort of terms can be hardcoded here, we will added this to the backend at some point.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
