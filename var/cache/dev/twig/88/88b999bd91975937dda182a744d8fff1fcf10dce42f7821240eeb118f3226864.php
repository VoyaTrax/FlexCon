<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_04e653a9d7a845e1f7c4bba506949a624b368de566b85058bf37ee9e27dd2e2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a09379080a9b7e75987424f022a0c479a8e0bac1169223dcc844f3241712e94 = $this->env->getExtension("native_profiler");
        $__internal_4a09379080a9b7e75987424f022a0c479a8e0bac1169223dcc844f3241712e94->enter($__internal_4a09379080a9b7e75987424f022a0c479a8e0bac1169223dcc844f3241712e94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_4a09379080a9b7e75987424f022a0c479a8e0bac1169223dcc844f3241712e94->leave($__internal_4a09379080a9b7e75987424f022a0c479a8e0bac1169223dcc844f3241712e94_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_61a81741881c4e8f3b933cf62ace4df274047fd866ae89f1266ed1159b270035 = $this->env->getExtension("native_profiler");
        $__internal_61a81741881c4e8f3b933cf62ace4df274047fd866ae89f1266ed1159b270035->enter($__internal_61a81741881c4e8f3b933cf62ace4df274047fd866ae89f1266ed1159b270035_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_61a81741881c4e8f3b933cf62ace4df274047fd866ae89f1266ed1159b270035->leave($__internal_61a81741881c4e8f3b933cf62ace4df274047fd866ae89f1266ed1159b270035_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
