<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_d7f93f0ae14bb81612668cca1e2d58303c60e98c36ba13fe0ecc132c0d3985d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ca4afa157fd8e3a8c18835f9f2c8dd99fc012ec048fae617db3bdd6feb1b8e9 = $this->env->getExtension("native_profiler");
        $__internal_8ca4afa157fd8e3a8c18835f9f2c8dd99fc012ec048fae617db3bdd6feb1b8e9->enter($__internal_8ca4afa157fd8e3a8c18835f9f2c8dd99fc012ec048fae617db3bdd6feb1b8e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_8ca4afa157fd8e3a8c18835f9f2c8dd99fc012ec048fae617db3bdd6feb1b8e9->leave($__internal_8ca4afa157fd8e3a8c18835f9f2c8dd99fc012ec048fae617db3bdd6feb1b8e9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
