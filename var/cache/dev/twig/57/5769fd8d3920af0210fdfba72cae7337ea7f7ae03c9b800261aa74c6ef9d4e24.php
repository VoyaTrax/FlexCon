<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_8afd0ba15cf915d34765b032bfd0fc34652f4d2df655a3d18e4add802d8cc0fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2f57d41244aac6839910a5689dd62f9f88b17ff5cc9bd54261f328436827147 = $this->env->getExtension("native_profiler");
        $__internal_a2f57d41244aac6839910a5689dd62f9f88b17ff5cc9bd54261f328436827147->enter($__internal_a2f57d41244aac6839910a5689dd62f9f88b17ff5cc9bd54261f328436827147_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_a2f57d41244aac6839910a5689dd62f9f88b17ff5cc9bd54261f328436827147->leave($__internal_a2f57d41244aac6839910a5689dd62f9f88b17ff5cc9bd54261f328436827147_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
