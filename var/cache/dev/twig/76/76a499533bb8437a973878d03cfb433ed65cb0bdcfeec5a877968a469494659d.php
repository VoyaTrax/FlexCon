<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_0ff9e82a5602279fdc1142ca7ef999748240b38a0c5cc55e5a1d1a348df77708 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d23232fe1f39733614d7123e3c8e65becef4575f232eb2ccc12ab336b80693eb = $this->env->getExtension("native_profiler");
        $__internal_d23232fe1f39733614d7123e3c8e65becef4575f232eb2ccc12ab336b80693eb->enter($__internal_d23232fe1f39733614d7123e3c8e65becef4575f232eb2ccc12ab336b80693eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_d23232fe1f39733614d7123e3c8e65becef4575f232eb2ccc12ab336b80693eb->leave($__internal_d23232fe1f39733614d7123e3c8e65becef4575f232eb2ccc12ab336b80693eb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
