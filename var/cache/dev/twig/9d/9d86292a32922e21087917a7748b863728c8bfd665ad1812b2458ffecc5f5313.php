<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_275a8a8eeff64fad300a40bb90b91c025281d82259ac00795826c48745e5d8f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67682e091bcb800429a831f8bbe85f5694b4b7632e26f7c0d864a0a9432138a5 = $this->env->getExtension("native_profiler");
        $__internal_67682e091bcb800429a831f8bbe85f5694b4b7632e26f7c0d864a0a9432138a5->enter($__internal_67682e091bcb800429a831f8bbe85f5694b4b7632e26f7c0d864a0a9432138a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_67682e091bcb800429a831f8bbe85f5694b4b7632e26f7c0d864a0a9432138a5->leave($__internal_67682e091bcb800429a831f8bbe85f5694b4b7632e26f7c0d864a0a9432138a5_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
