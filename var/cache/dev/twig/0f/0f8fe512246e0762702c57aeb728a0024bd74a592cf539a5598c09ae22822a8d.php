<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_98bb1a9947bde5cae46be6a5805b8afdb37264491e348b92c75b5fe3ba6f2127 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6a202dd542e2eef023eb53a6531e45fa753e03d4886485c98180d5910d4c118 = $this->env->getExtension("native_profiler");
        $__internal_d6a202dd542e2eef023eb53a6531e45fa753e03d4886485c98180d5910d4c118->enter($__internal_d6a202dd542e2eef023eb53a6531e45fa753e03d4886485c98180d5910d4c118_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_d6a202dd542e2eef023eb53a6531e45fa753e03d4886485c98180d5910d4c118->leave($__internal_d6a202dd542e2eef023eb53a6531e45fa753e03d4886485c98180d5910d4c118_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
