<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_fd7b40d8802830df58b1243d755635848805205a82f95f1e3af360c9db88b018 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16ac42c8ddb2ed93ae8c341bb0b559a08a2e0b1491d98bd5ef4aa6385b3f08b5 = $this->env->getExtension("native_profiler");
        $__internal_16ac42c8ddb2ed93ae8c341bb0b559a08a2e0b1491d98bd5ef4aa6385b3f08b5->enter($__internal_16ac42c8ddb2ed93ae8c341bb0b559a08a2e0b1491d98bd5ef4aa6385b3f08b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_16ac42c8ddb2ed93ae8c341bb0b559a08a2e0b1491d98bd5ef4aa6385b3f08b5->leave($__internal_16ac42c8ddb2ed93ae8c341bb0b559a08a2e0b1491d98bd5ef4aa6385b3f08b5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
