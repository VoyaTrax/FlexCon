<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_43ab84a5012d1ff7b36806102310b58471c5b1eaef842d43ef9be591d891fe95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6d031ac9d44a54f1a9b256b86ba2d013de80b436d1df230019d3f4e63db2105 = $this->env->getExtension("native_profiler");
        $__internal_e6d031ac9d44a54f1a9b256b86ba2d013de80b436d1df230019d3f4e63db2105->enter($__internal_e6d031ac9d44a54f1a9b256b86ba2d013de80b436d1df230019d3f4e63db2105_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_e6d031ac9d44a54f1a9b256b86ba2d013de80b436d1df230019d3f4e63db2105->leave($__internal_e6d031ac9d44a54f1a9b256b86ba2d013de80b436d1df230019d3f4e63db2105_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
