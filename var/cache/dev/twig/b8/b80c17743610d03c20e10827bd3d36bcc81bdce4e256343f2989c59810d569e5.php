<?php

/* FlexConCmsBundle:Page:license.html.twig */
class __TwigTemplate_adc403f4f846313cdba875fb83761ae49d735313fe41c57d5251ce6b1eb9bd24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:license.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c716a5be1d388a6bfae5887f311520da1d7004384b495c1efd2dd6663e284fc9 = $this->env->getExtension("native_profiler");
        $__internal_c716a5be1d388a6bfae5887f311520da1d7004384b495c1efd2dd6663e284fc9->enter($__internal_c716a5be1d388a6bfae5887f311520da1d7004384b495c1efd2dd6663e284fc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:license.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c716a5be1d388a6bfae5887f311520da1d7004384b495c1efd2dd6663e284fc9->leave($__internal_c716a5be1d388a6bfae5887f311520da1d7004384b495c1efd2dd6663e284fc9_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_e3136121ad923173ce941ff7b5bef6b007cd9422326119d529bad51480de6be3 = $this->env->getExtension("native_profiler");
        $__internal_e3136121ad923173ce941ff7b5bef6b007cd9422326119d529bad51480de6be3->enter($__internal_e3136121ad923173ce941ff7b5bef6b007cd9422326119d529bad51480de6be3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "License(s)";
        
        $__internal_e3136121ad923173ce941ff7b5bef6b007cd9422326119d529bad51480de6be3->leave($__internal_e3136121ad923173ce941ff7b5bef6b007cd9422326119d529bad51480de6be3_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_34144ec6c85fec29861af7bc9a119a4707367f549d587af3aaa432d51141cbef = $this->env->getExtension("native_profiler");
        $__internal_34144ec6c85fec29861af7bc9a119a4707367f549d587af3aaa432d51141cbef->enter($__internal_34144ec6c85fec29861af7bc9a119a4707367f549d587af3aaa432d51141cbef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>License(s) used</h1>
    </header>
    <article>
        <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas
        condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis
        vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a
        lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
        posuere cubilia Curae.</p>
    </article>
";
        
        $__internal_34144ec6c85fec29861af7bc9a119a4707367f549d587af3aaa432d51141cbef->leave($__internal_34144ec6c85fec29861af7bc9a119a4707367f549d587af3aaa432d51141cbef_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:license.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/license.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}License(s){% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>License(s) used</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas*/
/*         condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis*/
/*         vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a*/
/*         lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices*/
/*         posuere cubilia Curae.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
