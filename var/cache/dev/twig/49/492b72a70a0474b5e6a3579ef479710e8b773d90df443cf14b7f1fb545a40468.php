<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_65a61065905b9492a7ca9efb6f95d368f9a11308c842151606c9690a7d318d6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a02ed9301d6a8ece46a7f11604d9cdc9157cc58f878f7dabfc19b27c1a06d895 = $this->env->getExtension("native_profiler");
        $__internal_a02ed9301d6a8ece46a7f11604d9cdc9157cc58f878f7dabfc19b27c1a06d895->enter($__internal_a02ed9301d6a8ece46a7f11604d9cdc9157cc58f878f7dabfc19b27c1a06d895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_a02ed9301d6a8ece46a7f11604d9cdc9157cc58f878f7dabfc19b27c1a06d895->leave($__internal_a02ed9301d6a8ece46a7f11604d9cdc9157cc58f878f7dabfc19b27c1a06d895_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
