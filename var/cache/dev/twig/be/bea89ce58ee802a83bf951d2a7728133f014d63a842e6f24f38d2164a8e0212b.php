<?php

/* FlexConCmsBundle:Page:terms.html.twig */
class __TwigTemplate_86f24615484c3034a9a8839622778ec96704e914ace1bac223901f5d1a509e8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:terms.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_147d156c49d66b66f2f05a321bc4f5921fb6f852a2c183810a7a02b4581f18bf = $this->env->getExtension("native_profiler");
        $__internal_147d156c49d66b66f2f05a321bc4f5921fb6f852a2c183810a7a02b4581f18bf->enter($__internal_147d156c49d66b66f2f05a321bc4f5921fb6f852a2c183810a7a02b4581f18bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:terms.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_147d156c49d66b66f2f05a321bc4f5921fb6f852a2c183810a7a02b4581f18bf->leave($__internal_147d156c49d66b66f2f05a321bc4f5921fb6f852a2c183810a7a02b4581f18bf_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_881c11a25c308ce2487b9f1c9f79ab59e3a1ee91c253a2e592e6d85d8746326d = $this->env->getExtension("native_profiler");
        $__internal_881c11a25c308ce2487b9f1c9f79ab59e3a1ee91c253a2e592e6d85d8746326d->enter($__internal_881c11a25c308ce2487b9f1c9f79ab59e3a1ee91c253a2e592e6d85d8746326d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Terms";
        
        $__internal_881c11a25c308ce2487b9f1c9f79ab59e3a1ee91c253a2e592e6d85d8746326d->leave($__internal_881c11a25c308ce2487b9f1c9f79ab59e3a1ee91c253a2e592e6d85d8746326d_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_f0477c7df6b1358c5e981043b08c4db6599c46b553c9d7b244f77a277cc26b13 = $this->env->getExtension("native_profiler");
        $__internal_f0477c7df6b1358c5e981043b08c4db6599c46b553c9d7b244f77a277cc26b13->enter($__internal_f0477c7df6b1358c5e981043b08c4db6599c46b553c9d7b244f77a277cc26b13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Terms of Use</h1>
    </header>
    <article>
        <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas
        condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis
        vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a
        lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
        posuere cubilia Curae.</p>
    </article>
";
        
        $__internal_f0477c7df6b1358c5e981043b08c4db6599c46b553c9d7b244f77a277cc26b13->leave($__internal_f0477c7df6b1358c5e981043b08c4db6599c46b553c9d7b244f77a277cc26b13_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:terms.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/terms.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Terms{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Terms of Use</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas*/
/*         condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis*/
/*         vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a*/
/*         lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices*/
/*         posuere cubilia Curae.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
