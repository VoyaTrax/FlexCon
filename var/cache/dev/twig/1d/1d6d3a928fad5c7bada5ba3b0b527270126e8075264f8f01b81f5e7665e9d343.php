<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_24241ab20a8e03e33408ab0d9e3c1d0ae91f856efbf1617a52903466bb7b1a15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d26e05634b99dc4aed110c583038c089723d848024f203e25688138289e84e79 = $this->env->getExtension("native_profiler");
        $__internal_d26e05634b99dc4aed110c583038c089723d848024f203e25688138289e84e79->enter($__internal_d26e05634b99dc4aed110c583038c089723d848024f203e25688138289e84e79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_d26e05634b99dc4aed110c583038c089723d848024f203e25688138289e84e79->leave($__internal_d26e05634b99dc4aed110c583038c089723d848024f203e25688138289e84e79_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
