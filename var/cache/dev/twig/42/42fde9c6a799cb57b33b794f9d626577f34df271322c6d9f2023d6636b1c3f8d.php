<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_5ff42597aa19de7aa3b750c392892ea8102d32312b14c2ed01a5e6bd6c52eff3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0521fc27dc48f14f32ba4cb0be21956e392ed5fdd64608f1006e9ea8c60c880 = $this->env->getExtension("native_profiler");
        $__internal_e0521fc27dc48f14f32ba4cb0be21956e392ed5fdd64608f1006e9ea8c60c880->enter($__internal_e0521fc27dc48f14f32ba4cb0be21956e392ed5fdd64608f1006e9ea8c60c880_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_e0521fc27dc48f14f32ba4cb0be21956e392ed5fdd64608f1006e9ea8c60c880->leave($__internal_e0521fc27dc48f14f32ba4cb0be21956e392ed5fdd64608f1006e9ea8c60c880_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
