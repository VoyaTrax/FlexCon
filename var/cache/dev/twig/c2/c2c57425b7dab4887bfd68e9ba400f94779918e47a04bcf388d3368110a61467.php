<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_6cbb021bf08f40e6f307e436bf89ac8a466ba082a19a95656d4fd6d5df7045e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d5c99670124d4ac38ea1202b68ca589c9d1bf4f173ec0663dd6ecf9baebdda7e = $this->env->getExtension("native_profiler");
        $__internal_d5c99670124d4ac38ea1202b68ca589c9d1bf4f173ec0663dd6ecf9baebdda7e->enter($__internal_d5c99670124d4ac38ea1202b68ca589c9d1bf4f173ec0663dd6ecf9baebdda7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_d5c99670124d4ac38ea1202b68ca589c9d1bf4f173ec0663dd6ecf9baebdda7e->leave($__internal_d5c99670124d4ac38ea1202b68ca589c9d1bf4f173ec0663dd6ecf9baebdda7e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
