<?php

/* FlexConCmsBundle::layout.html.twig */
class __TwigTemplate_fd678b1b5dc85569df473703a70fa855a0ee9f5a1bcf067927dad918dcd9fbb6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "FlexConCmsBundle::layout.html.twig", 2);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_374b8c708e940640cfc14b278f79ebb3124c483b7092c2c8d6a9d3ec9e232f43 = $this->env->getExtension("native_profiler");
        $__internal_374b8c708e940640cfc14b278f79ebb3124c483b7092c2c8d6a9d3ec9e232f43->enter($__internal_374b8c708e940640cfc14b278f79ebb3124c483b7092c2c8d6a9d3ec9e232f43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_374b8c708e940640cfc14b278f79ebb3124c483b7092c2c8d6a9d3ec9e232f43->leave($__internal_374b8c708e940640cfc14b278f79ebb3124c483b7092c2c8d6a9d3ec9e232f43_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_71a73eccdf8ae7f93685a0f0fc6d408abef1674f83db57302b12ba0d4daf7972 = $this->env->getExtension("native_profiler");
        $__internal_71a73eccdf8ae7f93685a0f0fc6d408abef1674f83db57302b12ba0d4daf7972->enter($__internal_71a73eccdf8ae7f93685a0f0fc6d408abef1674f83db57302b12ba0d4daf7972_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/flexconcms/css/blog.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
    
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/flexconcms/css/sidebar.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
";
        
        $__internal_71a73eccdf8ae7f93685a0f0fc6d408abef1674f83db57302b12ba0d4daf7972->leave($__internal_71a73eccdf8ae7f93685a0f0fc6d408abef1674f83db57302b12ba0d4daf7972_prof);

    }

    // line 13
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_0ffeda0025fc426ffb73b7c6937d0c4c42bef806fe57e5696cb461fc69bbe145 = $this->env->getExtension("native_profiler");
        $__internal_0ffeda0025fc426ffb73b7c6937d0c4c42bef806fe57e5696cb461fc69bbe145->enter($__internal_0ffeda0025fc426ffb73b7c6937d0c4c42bef806fe57e5696cb461fc69bbe145_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 14
        echo "   ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FlexConCmsBundle:Page:sidebar"));
        echo "
";
        
        $__internal_0ffeda0025fc426ffb73b7c6937d0c4c42bef806fe57e5696cb461fc69bbe145->leave($__internal_0ffeda0025fc426ffb73b7c6937d0c4c42bef806fe57e5696cb461fc69bbe145_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 14,  60 => 13,  51 => 8,  46 => 6,  41 => 5,  35 => 4,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/layout.html.twig #}*/
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/*     <link href="{{ asset('bundles/flexconcms/css/blog.css') }}" type="text/css" rel="stylesheet" />*/
/*     */
/*     <link href="{{ asset('bundles/flexconcms/css/sidebar.css') }}" type="text/css" rel="stylesheet" />*/
/* {% endblock %}*/
/* */
/* */
/* */
/* {% block sidebar %}*/
/*    {{ render(controller( 'FlexConCmsBundle:Page:sidebar' )) }}*/
/* {% endblock %}*/
/* */
/* */
