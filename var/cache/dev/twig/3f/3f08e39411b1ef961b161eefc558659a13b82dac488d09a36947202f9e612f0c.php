<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_c7f4427b76d842cba5438881044e7d471557a87948fac5397a3a0e2bf3c05ff5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_437baf4b034d16a22306de8c98c3e9f9cfd47e36336fa8651168394b9783e96b = $this->env->getExtension("native_profiler");
        $__internal_437baf4b034d16a22306de8c98c3e9f9cfd47e36336fa8651168394b9783e96b->enter($__internal_437baf4b034d16a22306de8c98c3e9f9cfd47e36336fa8651168394b9783e96b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_437baf4b034d16a22306de8c98c3e9f9cfd47e36336fa8651168394b9783e96b->leave($__internal_437baf4b034d16a22306de8c98c3e9f9cfd47e36336fa8651168394b9783e96b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
