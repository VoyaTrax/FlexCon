<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_7715abe0e4b8766d5e5870d747b0f364e2b021906d6aecb54c61ab04d8e91188 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2f5969f60c1857543a55fa163774ab4e31ca49c4ec29fe610662f2afee0a39e = $this->env->getExtension("native_profiler");
        $__internal_d2f5969f60c1857543a55fa163774ab4e31ca49c4ec29fe610662f2afee0a39e->enter($__internal_d2f5969f60c1857543a55fa163774ab4e31ca49c4ec29fe610662f2afee0a39e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_d2f5969f60c1857543a55fa163774ab4e31ca49c4ec29fe610662f2afee0a39e->leave($__internal_d2f5969f60c1857543a55fa163774ab4e31ca49c4ec29fe610662f2afee0a39e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
