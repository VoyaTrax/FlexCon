<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_881d7e9c2b84a19f454a8eb2b75c75b3b9e44499c731a90741beeeef3217054f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d5fade39c1349308815c20e8fc33dcccda8e92d33b4625df02a64e5555ed8012 = $this->env->getExtension("native_profiler");
        $__internal_d5fade39c1349308815c20e8fc33dcccda8e92d33b4625df02a64e5555ed8012->enter($__internal_d5fade39c1349308815c20e8fc33dcccda8e92d33b4625df02a64e5555ed8012_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_d5fade39c1349308815c20e8fc33dcccda8e92d33b4625df02a64e5555ed8012->leave($__internal_d5fade39c1349308815c20e8fc33dcccda8e92d33b4625df02a64e5555ed8012_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
