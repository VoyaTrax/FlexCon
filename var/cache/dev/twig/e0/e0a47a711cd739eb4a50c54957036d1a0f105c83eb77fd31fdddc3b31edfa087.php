<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_e418d015ff9335e66fd31bf74bc0ac7c0c588fad22391d3e040b689779dfba30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd216a3d62f05dc7c3fe8cb58f9709a0d0fac3a9ec3e48630b04c54942da2462 = $this->env->getExtension("native_profiler");
        $__internal_bd216a3d62f05dc7c3fe8cb58f9709a0d0fac3a9ec3e48630b04c54942da2462->enter($__internal_bd216a3d62f05dc7c3fe8cb58f9709a0d0fac3a9ec3e48630b04c54942da2462_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_bd216a3d62f05dc7c3fe8cb58f9709a0d0fac3a9ec3e48630b04c54942da2462->leave($__internal_bd216a3d62f05dc7c3fe8cb58f9709a0d0fac3a9ec3e48630b04c54942da2462_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
