<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_488853e1b708db50d01dd13edda63f13407fe61642874d7a567c12f896149155 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81cb3f6ecc64c55083246b59fbd4cc5b5e1dbf660148ccc86332c80739b5021f = $this->env->getExtension("native_profiler");
        $__internal_81cb3f6ecc64c55083246b59fbd4cc5b5e1dbf660148ccc86332c80739b5021f->enter($__internal_81cb3f6ecc64c55083246b59fbd4cc5b5e1dbf660148ccc86332c80739b5021f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_81cb3f6ecc64c55083246b59fbd4cc5b5e1dbf660148ccc86332c80739b5021f->leave($__internal_81cb3f6ecc64c55083246b59fbd4cc5b5e1dbf660148ccc86332c80739b5021f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
