<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_99e77d8af319b594fd47b41ea1af887b70b02044002d89c10949fda8ef4f9bac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5981f13b17fe2542b49d6f21456e7f7e3462e060d6ad4ab0431749d84da1bb1b = $this->env->getExtension("native_profiler");
        $__internal_5981f13b17fe2542b49d6f21456e7f7e3462e060d6ad4ab0431749d84da1bb1b->enter($__internal_5981f13b17fe2542b49d6f21456e7f7e3462e060d6ad4ab0431749d84da1bb1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_5981f13b17fe2542b49d6f21456e7f7e3462e060d6ad4ab0431749d84da1bb1b->leave($__internal_5981f13b17fe2542b49d6f21456e7f7e3462e060d6ad4ab0431749d84da1bb1b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
