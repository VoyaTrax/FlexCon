<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_cdc741c953e8eb6187e0078f35213511812cd715cdd87077aae1a754c15c07c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_783b28c8e1a37cbe122388e8093895c84e422aa62a42b1f1e0bade177e9514e1 = $this->env->getExtension("native_profiler");
        $__internal_783b28c8e1a37cbe122388e8093895c84e422aa62a42b1f1e0bade177e9514e1->enter($__internal_783b28c8e1a37cbe122388e8093895c84e422aa62a42b1f1e0bade177e9514e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_783b28c8e1a37cbe122388e8093895c84e422aa62a42b1f1e0bade177e9514e1->leave($__internal_783b28c8e1a37cbe122388e8093895c84e422aa62a42b1f1e0bade177e9514e1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
