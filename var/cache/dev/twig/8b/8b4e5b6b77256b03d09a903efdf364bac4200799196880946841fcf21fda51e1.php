<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e4341bb7a77bca3b03a5f4647d9e8436a7bd32ed6c0ee48f27d91f0424a00b4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_292dc2bd311fc92af397093d1e2e9d16c4e9fe461be0c30fbba3ba89951bfd6a = $this->env->getExtension("native_profiler");
        $__internal_292dc2bd311fc92af397093d1e2e9d16c4e9fe461be0c30fbba3ba89951bfd6a->enter($__internal_292dc2bd311fc92af397093d1e2e9d16c4e9fe461be0c30fbba3ba89951bfd6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_292dc2bd311fc92af397093d1e2e9d16c4e9fe461be0c30fbba3ba89951bfd6a->leave($__internal_292dc2bd311fc92af397093d1e2e9d16c4e9fe461be0c30fbba3ba89951bfd6a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8fc974e139996ed6bd35c003d3c949ca6b7822eae678d97b16c0532b304de067 = $this->env->getExtension("native_profiler");
        $__internal_8fc974e139996ed6bd35c003d3c949ca6b7822eae678d97b16c0532b304de067->enter($__internal_8fc974e139996ed6bd35c003d3c949ca6b7822eae678d97b16c0532b304de067_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_8fc974e139996ed6bd35c003d3c949ca6b7822eae678d97b16c0532b304de067->leave($__internal_8fc974e139996ed6bd35c003d3c949ca6b7822eae678d97b16c0532b304de067_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_83c810431789945d843f624745927b19f4e5582f3c49ff8eefd90440d9b85838 = $this->env->getExtension("native_profiler");
        $__internal_83c810431789945d843f624745927b19f4e5582f3c49ff8eefd90440d9b85838->enter($__internal_83c810431789945d843f624745927b19f4e5582f3c49ff8eefd90440d9b85838_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_83c810431789945d843f624745927b19f4e5582f3c49ff8eefd90440d9b85838->leave($__internal_83c810431789945d843f624745927b19f4e5582f3c49ff8eefd90440d9b85838_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_a73a6e891226b12a498a94baaf90f8aa07fa781fcacb86aaf22e87b318ba1462 = $this->env->getExtension("native_profiler");
        $__internal_a73a6e891226b12a498a94baaf90f8aa07fa781fcacb86aaf22e87b318ba1462->enter($__internal_a73a6e891226b12a498a94baaf90f8aa07fa781fcacb86aaf22e87b318ba1462_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_a73a6e891226b12a498a94baaf90f8aa07fa781fcacb86aaf22e87b318ba1462->leave($__internal_a73a6e891226b12a498a94baaf90f8aa07fa781fcacb86aaf22e87b318ba1462_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
