<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_172689f07fc965e75967b165b9fce7aa19bf5261f5cafa7479b5ce9acd5328a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82a2dedd3ba4a8e32edeb9ed34f119bda8a955feeb1594bc4d4518d8d486085d = $this->env->getExtension("native_profiler");
        $__internal_82a2dedd3ba4a8e32edeb9ed34f119bda8a955feeb1594bc4d4518d8d486085d->enter($__internal_82a2dedd3ba4a8e32edeb9ed34f119bda8a955feeb1594bc4d4518d8d486085d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_82a2dedd3ba4a8e32edeb9ed34f119bda8a955feeb1594bc4d4518d8d486085d->leave($__internal_82a2dedd3ba4a8e32edeb9ed34f119bda8a955feeb1594bc4d4518d8d486085d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
