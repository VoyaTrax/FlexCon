<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_e9712cbe3d26075131d3b88b88f750c8feceb81a65ed0f26668492c7f698aacc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc27f073f2fa220d66a3494dc6507c3d2c3ba664f1f04a277f3b8364a3ace6c7 = $this->env->getExtension("native_profiler");
        $__internal_cc27f073f2fa220d66a3494dc6507c3d2c3ba664f1f04a277f3b8364a3ace6c7->enter($__internal_cc27f073f2fa220d66a3494dc6507c3d2c3ba664f1f04a277f3b8364a3ace6c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_cc27f073f2fa220d66a3494dc6507c3d2c3ba664f1f04a277f3b8364a3ace6c7->leave($__internal_cc27f073f2fa220d66a3494dc6507c3d2c3ba664f1f04a277f3b8364a3ace6c7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
