<?php

/* ::base.html.twig */
class __TwigTemplate_df3ce5c97372a8e4725d26f850ec4c11081e218dbe60ca971b3c26bb0d827f93 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'blog_title' => array($this, 'block_blog_title'),
            'blog_tagline' => array($this, 'block_blog_tagline'),
            'body' => array($this, 'block_body'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b49c02355f5c94c6093d6c89f5a1e9270ac78155fc8eff4c3fc049050310602 = $this->env->getExtension("native_profiler");
        $__internal_0b49c02355f5c94c6093d6c89f5a1e9270ac78155fc8eff4c3fc049050310602->enter($__internal_0b49c02355f5c94c6093d6c89f5a1e9270ac78155fc8eff4c3fc049050310602_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!-- app/Resources/views/base.html.twig -->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html\"; charset=utf-8\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo " - FlexCon</title>
        <!--[if lt IE 9]>
            <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
        <![endif]-->
        ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "        <link rel=\"shortcut icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        <section id=\"wrapper\">
            <header id=\"header\">
                <div class=\"top\">
                    ";
        // line 22
        $this->displayBlock('navigation', $context, $blocks);
        // line 31
        echo "                </div>

                <hgroup>
                    <h2>";
        // line 34
        $this->displayBlock('blog_title', $context, $blocks);
        echo "</h2>
                    <h3>";
        // line 35
        $this->displayBlock('blog_tagline', $context, $blocks);
        echo "</h3>
                </hgroup>
            </header>

            <section class=\"main-col\">
                ";
        // line 40
        $this->displayBlock('body', $context, $blocks);
        // line 41
        echo "            </section>
            <aside class=\"sidebar\">
                ";
        // line 43
        $this->displayBlock('sidebar', $context, $blocks);
        // line 44
        echo "            </aside>

            <div id=\"footer\">
                ";
        // line 47
        $this->displayBlock('footer', $context, $blocks);
        // line 51
        echo "            </div>
        </section>

        ";
        // line 54
        $this->displayBlock('javascripts', $context, $blocks);
        // line 55
        echo "    </body>
</html>
";
        
        $__internal_0b49c02355f5c94c6093d6c89f5a1e9270ac78155fc8eff4c3fc049050310602->leave($__internal_0b49c02355f5c94c6093d6c89f5a1e9270ac78155fc8eff4c3fc049050310602_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_a54e2a5fbd057c13edbc9c67f572a9448641557c735291f064f5ebb920322c7f = $this->env->getExtension("native_profiler");
        $__internal_a54e2a5fbd057c13edbc9c67f572a9448641557c735291f064f5ebb920322c7f->enter($__internal_a54e2a5fbd057c13edbc9c67f572a9448641557c735291f064f5ebb920322c7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "VoyaTrax Project";
        
        $__internal_a54e2a5fbd057c13edbc9c67f572a9448641557c735291f064f5ebb920322c7f->leave($__internal_a54e2a5fbd057c13edbc9c67f572a9448641557c735291f064f5ebb920322c7f_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_c3a91ec0d02762a467deeafd1ace261f00ea7d436c277688108a4546217cd317 = $this->env->getExtension("native_profiler");
        $__internal_c3a91ec0d02762a467deeafd1ace261f00ea7d436c277688108a4546217cd317->enter($__internal_c3a91ec0d02762a467deeafd1ace261f00ea7d436c277688108a4546217cd317_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "            <link href='http://fonts.googleapis.com/css?family=Irish+Grover' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=La+Belle+Aurore' rel='stylesheet' type='text/css'>
            <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/screen.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
        ";
        
        $__internal_c3a91ec0d02762a467deeafd1ace261f00ea7d436c277688108a4546217cd317->leave($__internal_c3a91ec0d02762a467deeafd1ace261f00ea7d436c277688108a4546217cd317_prof);

    }

    // line 22
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_f28e93055f9124a7c7ea681eafb6156291cbab6bcc191e7bbefeb872200e38bd = $this->env->getExtension("native_profiler");
        $__internal_f28e93055f9124a7c7ea681eafb6156291cbab6bcc191e7bbefeb872200e38bd->enter($__internal_f28e93055f9124a7c7ea681eafb6156291cbab6bcc191e7bbefeb872200e38bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 23
        echo "                        <nav>
                            <ul class=\"navigation\">
                                <li><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_homepage");
        echo "\">Home</a></li>
                                <li><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_about");
        echo "\">About</a></li>
                                <li><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_contact");
        echo "\">Contact</a></li>
                            </ul>
                        </nav>
                    ";
        
        $__internal_f28e93055f9124a7c7ea681eafb6156291cbab6bcc191e7bbefeb872200e38bd->leave($__internal_f28e93055f9124a7c7ea681eafb6156291cbab6bcc191e7bbefeb872200e38bd_prof);

    }

    // line 34
    public function block_blog_title($context, array $blocks = array())
    {
        $__internal_e47b343518cd41f01110239c2010903faa479a4014e497f8dcc9aed2881d6c26 = $this->env->getExtension("native_profiler");
        $__internal_e47b343518cd41f01110239c2010903faa479a4014e497f8dcc9aed2881d6c26->enter($__internal_e47b343518cd41f01110239c2010903faa479a4014e497f8dcc9aed2881d6c26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "blog_title"));

        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_homepage");
        echo "\">FlexCon</a>";
        
        $__internal_e47b343518cd41f01110239c2010903faa479a4014e497f8dcc9aed2881d6c26->leave($__internal_e47b343518cd41f01110239c2010903faa479a4014e497f8dcc9aed2881d6c26_prof);

    }

    // line 35
    public function block_blog_tagline($context, array $blocks = array())
    {
        $__internal_caceafb9e290ad3799ce75206b6c4bcc2d744a866284020be0a52086fb7ca2ca = $this->env->getExtension("native_profiler");
        $__internal_caceafb9e290ad3799ce75206b6c4bcc2d744a866284020be0a52086fb7ca2ca->enter($__internal_caceafb9e290ad3799ce75206b6c4bcc2d744a866284020be0a52086fb7ca2ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "blog_tagline"));

        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_homepage");
        echo "\">FUN | FAST | SECURE'</a>";
        
        $__internal_caceafb9e290ad3799ce75206b6c4bcc2d744a866284020be0a52086fb7ca2ca->leave($__internal_caceafb9e290ad3799ce75206b6c4bcc2d744a866284020be0a52086fb7ca2ca_prof);

    }

    // line 40
    public function block_body($context, array $blocks = array())
    {
        $__internal_536cb66a0d3b0c445d5839c75629884c03995b576706bec21edb7e13aa015800 = $this->env->getExtension("native_profiler");
        $__internal_536cb66a0d3b0c445d5839c75629884c03995b576706bec21edb7e13aa015800->enter($__internal_536cb66a0d3b0c445d5839c75629884c03995b576706bec21edb7e13aa015800_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_536cb66a0d3b0c445d5839c75629884c03995b576706bec21edb7e13aa015800->leave($__internal_536cb66a0d3b0c445d5839c75629884c03995b576706bec21edb7e13aa015800_prof);

    }

    // line 43
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_5066c40387916a10b5c714563b32fcfa8c83806370a52e003e5c4021134d563e = $this->env->getExtension("native_profiler");
        $__internal_5066c40387916a10b5c714563b32fcfa8c83806370a52e003e5c4021134d563e->enter($__internal_5066c40387916a10b5c714563b32fcfa8c83806370a52e003e5c4021134d563e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        
        $__internal_5066c40387916a10b5c714563b32fcfa8c83806370a52e003e5c4021134d563e->leave($__internal_5066c40387916a10b5c714563b32fcfa8c83806370a52e003e5c4021134d563e_prof);

    }

    // line 47
    public function block_footer($context, array $blocks = array())
    {
        $__internal_ea5c4b5d54066d594c9b440f992ce7e7fba7dfd433618e6bc7074a69416f264f = $this->env->getExtension("native_profiler");
        $__internal_ea5c4b5d54066d594c9b440f992ce7e7fba7dfd433618e6bc7074a69416f264f->enter($__internal_ea5c4b5d54066d594c9b440f992ce7e7fba7dfd433618e6bc7074a69416f264f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 48
        echo "                    FlexCon &copy; ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " <a href=\"http://jmcoleman.eu\" target=\"_blank\">JMColeman</a> for the <a href=\"http://voyatrax.org\" target=\"_blank\">VoyaTrax Project</a><br> <font size=\"-2\">Based on Symfony2 blog tutorial - created by <a href=\"https://github.com/dsyph3r\" target=\"_blank\">dsyph3r</a></font><br><br>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_terms");
        echo "\">Terms of Use</a> | <a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_policy");
        echo "\">Privacy Policy</a> | <a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_license");
        echo "\">License(s)</a> | <a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_imprint");
        echo "\">Imprint</a>
                ";
        
        $__internal_ea5c4b5d54066d594c9b440f992ce7e7fba7dfd433618e6bc7074a69416f264f->leave($__internal_ea5c4b5d54066d594c9b440f992ce7e7fba7dfd433618e6bc7074a69416f264f_prof);

    }

    // line 54
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_51b365727afafa9d132d797965c6d58e5eb3b04a3063494eedd4b668dd10102e = $this->env->getExtension("native_profiler");
        $__internal_51b365727afafa9d132d797965c6d58e5eb3b04a3063494eedd4b668dd10102e->enter($__internal_51b365727afafa9d132d797965c6d58e5eb3b04a3063494eedd4b668dd10102e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_51b365727afafa9d132d797965c6d58e5eb3b04a3063494eedd4b668dd10102e->leave($__internal_51b365727afafa9d132d797965c6d58e5eb3b04a3063494eedd4b668dd10102e_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 54,  229 => 49,  224 => 48,  218 => 47,  207 => 43,  196 => 40,  182 => 35,  168 => 34,  157 => 27,  153 => 26,  149 => 25,  145 => 23,  139 => 22,  130 => 13,  126 => 11,  120 => 10,  108 => 6,  99 => 55,  97 => 54,  92 => 51,  90 => 47,  85 => 44,  83 => 43,  79 => 41,  77 => 40,  69 => 35,  65 => 34,  60 => 31,  58 => 22,  47 => 15,  45 => 10,  38 => 6,  31 => 1,);
    }
}
/* <!-- app/Resources/views/base.html.twig -->*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta http-equiv="Content-Type" content="text/html"; charset=utf-8" />*/
/*         <title>{% block title %}VoyaTrax Project{% endblock %} - FlexCon</title>*/
/*         <!--[if lt IE 9]>*/
/*             <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/*         <![endif]-->*/
/*         {% block stylesheets %}*/
/*             <link href='http://fonts.googleapis.com/css?family=Irish+Grover' rel='stylesheet' type='text/css'>*/
/*             <link href='http://fonts.googleapis.com/css?family=La+Belle+Aurore' rel='stylesheet' type='text/css'>*/
/*             <link href="{{ asset('css/screen.css') }}" type="text/css" rel="stylesheet" />*/
/*         {% endblock %}*/
/*         <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/* */
/*         <section id="wrapper">*/
/*             <header id="header">*/
/*                 <div class="top">*/
/*                     {% block navigation %}*/
/*                         <nav>*/
/*                             <ul class="navigation">*/
/*                                 <li><a href="{{ path('flex_con_cms_homepage') }}">Home</a></li>*/
/*                                 <li><a href="{{ path('flex_con_cms_about') }}">About</a></li>*/
/*                                 <li><a href="{{ path('flex_con_cms_contact') }}">Contact</a></li>*/
/*                             </ul>*/
/*                         </nav>*/
/*                     {% endblock %}*/
/*                 </div>*/
/* */
/*                 <hgroup>*/
/*                     <h2>{% block blog_title %}<a href="{{ path('flex_con_cms_homepage') }}">FlexCon</a>{% endblock %}</h2>*/
/*                     <h3>{% block blog_tagline %}<a href="{{ path('flex_con_cms_homepage') }}">FUN | FAST | SECURE'</a>{% endblock %}</h3>*/
/*                 </hgroup>*/
/*             </header>*/
/* */
/*             <section class="main-col">*/
/*                 {% block body %}{% endblock %}*/
/*             </section>*/
/*             <aside class="sidebar">*/
/*                 {% block sidebar %}{% endblock %}*/
/*             </aside>*/
/* */
/*             <div id="footer">*/
/*                 {% block footer %}*/
/*                     FlexCon &copy; {{ "now"|date ("Y") }} <a href="http://jmcoleman.eu" target="_blank">JMColeman</a> for the <a href="http://voyatrax.org" target="_blank">VoyaTrax Project</a><br> <font size="-2">Based on Symfony2 blog tutorial - created by <a href="https://github.com/dsyph3r" target="_blank">dsyph3r</a></font><br><br>*/
/*                     <a href="{{ path('flex_con_cms_terms') }}">Terms of Use</a> | <a href="{{ path('flex_con_cms_policy') }}">Privacy Policy</a> | <a href="{{ path('flex_con_cms_license') }}">License(s)</a> | <a href="{{ path('flex_con_cms_imprint') }}">Imprint</a>*/
/*                 {% endblock %}*/
/*             </div>*/
/*         </section>*/
/* */
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
