<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_d9a0ca138d914958cd556352b55ce8d6293f8817a37886598c4dc2132ec835de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64097008d8e55bb2c303743b00d49ef92242fa270036896eeaa077f861c93fa3 = $this->env->getExtension("native_profiler");
        $__internal_64097008d8e55bb2c303743b00d49ef92242fa270036896eeaa077f861c93fa3->enter($__internal_64097008d8e55bb2c303743b00d49ef92242fa270036896eeaa077f861c93fa3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64097008d8e55bb2c303743b00d49ef92242fa270036896eeaa077f861c93fa3->leave($__internal_64097008d8e55bb2c303743b00d49ef92242fa270036896eeaa077f861c93fa3_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_dd0f18f8940e97e2bd485f7e14e400e1b743993f66a9b684763a7b519ad2528a = $this->env->getExtension("native_profiler");
        $__internal_dd0f18f8940e97e2bd485f7e14e400e1b743993f66a9b684763a7b519ad2528a->enter($__internal_dd0f18f8940e97e2bd485f7e14e400e1b743993f66a9b684763a7b519ad2528a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_dd0f18f8940e97e2bd485f7e14e400e1b743993f66a9b684763a7b519ad2528a->leave($__internal_dd0f18f8940e97e2bd485f7e14e400e1b743993f66a9b684763a7b519ad2528a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_d9d792c178672949e9c32f37a20349429787486c01e3fea5678c015927104e4f = $this->env->getExtension("native_profiler");
        $__internal_d9d792c178672949e9c32f37a20349429787486c01e3fea5678c015927104e4f->enter($__internal_d9d792c178672949e9c32f37a20349429787486c01e3fea5678c015927104e4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_d9d792c178672949e9c32f37a20349429787486c01e3fea5678c015927104e4f->leave($__internal_d9d792c178672949e9c32f37a20349429787486c01e3fea5678c015927104e4f_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_d2358e23a2ec142b65b47b41a9f63a916f1fd5c72551ceb0e2217f3a2db57664 = $this->env->getExtension("native_profiler");
        $__internal_d2358e23a2ec142b65b47b41a9f63a916f1fd5c72551ceb0e2217f3a2db57664->enter($__internal_d2358e23a2ec142b65b47b41a9f63a916f1fd5c72551ceb0e2217f3a2db57664_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_d2358e23a2ec142b65b47b41a9f63a916f1fd5c72551ceb0e2217f3a2db57664->leave($__internal_d2358e23a2ec142b65b47b41a9f63a916f1fd5c72551ceb0e2217f3a2db57664_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
