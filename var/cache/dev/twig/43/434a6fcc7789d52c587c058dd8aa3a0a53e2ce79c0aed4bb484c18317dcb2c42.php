<?php

/* @WebProfiler/Icon/no.svg */
class __TwigTemplate_021639b5017781e3f0eb7d513682db119b9f5bfe01b5a30cdd0fa3fce9a1a115 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31b48aeee34239f1a56a68ed70c8cc5f9763e29ecd61954d450d8f4e3c3d1c38 = $this->env->getExtension("native_profiler");
        $__internal_31b48aeee34239f1a56a68ed70c8cc5f9763e29ecd61954d450d8f4e3c3d1c38->enter($__internal_31b48aeee34239f1a56a68ed70c8cc5f9763e29ecd61954d450d8f4e3c3d1c38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
";
        
        $__internal_31b48aeee34239f1a56a68ed70c8cc5f9763e29ecd61954d450d8f4e3c3d1c38->leave($__internal_31b48aeee34239f1a56a68ed70c8cc5f9763e29ecd61954d450d8f4e3c3d1c38_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/no.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="28" height="28" viewBox="0 0 12 12" enable-background="new 0 0 12 12" xml:space="preserve">*/
/*     <path fill="#B0413E" d="M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4*/
/*     C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2*/
/*     C11.1,10,11.2,9.2,10.4,8.4z"/>*/
/* </svg>*/
/* */
