<?php

/* FlexConCmsBundle:Page:policy.html.twig */
class __TwigTemplate_eed27aa5cdd1508e18eda26474a2e1854fc58d77736d10a82f185010590a993c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:policy.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f3e97a312e9bd8159bedea0b8d9d52ebb095f1e470d34c61cbc23554bc212771 = $this->env->getExtension("native_profiler");
        $__internal_f3e97a312e9bd8159bedea0b8d9d52ebb095f1e470d34c61cbc23554bc212771->enter($__internal_f3e97a312e9bd8159bedea0b8d9d52ebb095f1e470d34c61cbc23554bc212771_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:policy.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f3e97a312e9bd8159bedea0b8d9d52ebb095f1e470d34c61cbc23554bc212771->leave($__internal_f3e97a312e9bd8159bedea0b8d9d52ebb095f1e470d34c61cbc23554bc212771_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_315d3f88031f43957d74bd0841766f53303480c6b624b7318f58d5ec0adb8947 = $this->env->getExtension("native_profiler");
        $__internal_315d3f88031f43957d74bd0841766f53303480c6b624b7318f58d5ec0adb8947->enter($__internal_315d3f88031f43957d74bd0841766f53303480c6b624b7318f58d5ec0adb8947_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Privacy Policy";
        
        $__internal_315d3f88031f43957d74bd0841766f53303480c6b624b7318f58d5ec0adb8947->leave($__internal_315d3f88031f43957d74bd0841766f53303480c6b624b7318f58d5ec0adb8947_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_d6c139dd43cca7dd269ff0aff518a1c97e523d1da191bcacce29ffb4b57f42f0 = $this->env->getExtension("native_profiler");
        $__internal_d6c139dd43cca7dd269ff0aff518a1c97e523d1da191bcacce29ffb4b57f42f0->enter($__internal_d6c139dd43cca7dd269ff0aff518a1c97e523d1da191bcacce29ffb4b57f42f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Privacy Policy</h1>
    </header>
    <article>
        <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas
        condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis
        vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a
        lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
        posuere cubilia Curae.</p>
    </article>
";
        
        $__internal_d6c139dd43cca7dd269ff0aff518a1c97e523d1da191bcacce29ffb4b57f42f0->leave($__internal_d6c139dd43cca7dd269ff0aff518a1c97e523d1da191bcacce29ffb4b57f42f0_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:policy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/policy.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Privacy Policy{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Privacy Policy</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas*/
/*         condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis*/
/*         vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a*/
/*         lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices*/
/*         posuere cubilia Curae.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
