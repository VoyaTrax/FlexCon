<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_cd570a4c80614807603898be3d800ad14afa47d4aab5010642c9623684cf9236 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_be51f6c5754f62d2a13467348f7d7b5b9f7ae72d86ee4951572fbc77976ed1a6 = $this->env->getExtension("native_profiler");
        $__internal_be51f6c5754f62d2a13467348f7d7b5b9f7ae72d86ee4951572fbc77976ed1a6->enter($__internal_be51f6c5754f62d2a13467348f7d7b5b9f7ae72d86ee4951572fbc77976ed1a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_be51f6c5754f62d2a13467348f7d7b5b9f7ae72d86ee4951572fbc77976ed1a6->leave($__internal_be51f6c5754f62d2a13467348f7d7b5b9f7ae72d86ee4951572fbc77976ed1a6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
