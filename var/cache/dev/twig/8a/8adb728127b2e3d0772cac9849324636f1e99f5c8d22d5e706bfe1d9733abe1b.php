<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_219eec4755b9091be287d7c512dda0578133e6ec8b38d977117b39c811ff54b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ecf13820bce297c27941668c70860d42754162274b156e2bc306e431120389b7 = $this->env->getExtension("native_profiler");
        $__internal_ecf13820bce297c27941668c70860d42754162274b156e2bc306e431120389b7->enter($__internal_ecf13820bce297c27941668c70860d42754162274b156e2bc306e431120389b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ecf13820bce297c27941668c70860d42754162274b156e2bc306e431120389b7->leave($__internal_ecf13820bce297c27941668c70860d42754162274b156e2bc306e431120389b7_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8d438878064e528a9469c59d56f9d40cc96c3b64ad57e78ed8d42c615474e759 = $this->env->getExtension("native_profiler");
        $__internal_8d438878064e528a9469c59d56f9d40cc96c3b64ad57e78ed8d42c615474e759->enter($__internal_8d438878064e528a9469c59d56f9d40cc96c3b64ad57e78ed8d42c615474e759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_8d438878064e528a9469c59d56f9d40cc96c3b64ad57e78ed8d42c615474e759->leave($__internal_8d438878064e528a9469c59d56f9d40cc96c3b64ad57e78ed8d42c615474e759_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_de166721b7dc93e9dbfbffea9b6bf7f78fdedebad431d82e7c1dcc42fc11a08a = $this->env->getExtension("native_profiler");
        $__internal_de166721b7dc93e9dbfbffea9b6bf7f78fdedebad431d82e7c1dcc42fc11a08a->enter($__internal_de166721b7dc93e9dbfbffea9b6bf7f78fdedebad431d82e7c1dcc42fc11a08a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_de166721b7dc93e9dbfbffea9b6bf7f78fdedebad431d82e7c1dcc42fc11a08a->leave($__internal_de166721b7dc93e9dbfbffea9b6bf7f78fdedebad431d82e7c1dcc42fc11a08a_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
