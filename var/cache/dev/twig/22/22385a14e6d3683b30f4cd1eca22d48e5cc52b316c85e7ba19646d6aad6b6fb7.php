<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_dc1e6afc86997ed3e038261e857b61c931080ebef454aab8321761c29699a8a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2741dc41e1692b878ec646e4d85ab5cdf9d643b08066bf150f018099da03524a = $this->env->getExtension("native_profiler");
        $__internal_2741dc41e1692b878ec646e4d85ab5cdf9d643b08066bf150f018099da03524a->enter($__internal_2741dc41e1692b878ec646e4d85ab5cdf9d643b08066bf150f018099da03524a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_2741dc41e1692b878ec646e4d85ab5cdf9d643b08066bf150f018099da03524a->leave($__internal_2741dc41e1692b878ec646e4d85ab5cdf9d643b08066bf150f018099da03524a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
