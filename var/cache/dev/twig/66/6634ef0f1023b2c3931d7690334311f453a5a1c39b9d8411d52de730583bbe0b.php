<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_a7b8168b7897567d8c4413125967d53057c2fc30726a8669bafc9a95a98d72f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63301a94bb3bc67fb2702a9451dea2b07a58ddb90ed9208bb8d41f0237be4c68 = $this->env->getExtension("native_profiler");
        $__internal_63301a94bb3bc67fb2702a9451dea2b07a58ddb90ed9208bb8d41f0237be4c68->enter($__internal_63301a94bb3bc67fb2702a9451dea2b07a58ddb90ed9208bb8d41f0237be4c68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_63301a94bb3bc67fb2702a9451dea2b07a58ddb90ed9208bb8d41f0237be4c68->leave($__internal_63301a94bb3bc67fb2702a9451dea2b07a58ddb90ed9208bb8d41f0237be4c68_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
