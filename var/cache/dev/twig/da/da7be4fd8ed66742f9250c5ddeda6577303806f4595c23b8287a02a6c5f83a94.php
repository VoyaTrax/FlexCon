<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_12361b04bd66e2f59150a9a6431eaf905835558373b7c8f89bfa2566c8622b15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17a1a278f576a2d9cd86c7919e15d88110b46f111a38dea4acb9c92e0b235ae0 = $this->env->getExtension("native_profiler");
        $__internal_17a1a278f576a2d9cd86c7919e15d88110b46f111a38dea4acb9c92e0b235ae0->enter($__internal_17a1a278f576a2d9cd86c7919e15d88110b46f111a38dea4acb9c92e0b235ae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_17a1a278f576a2d9cd86c7919e15d88110b46f111a38dea4acb9c92e0b235ae0->leave($__internal_17a1a278f576a2d9cd86c7919e15d88110b46f111a38dea4acb9c92e0b235ae0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
