<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_351ed53c74eaf10cc05da0ea002d74a474afd3abb52d722468f99d21a8d5a2f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab6b3197999686777ad7f253b026ead8fb87f87d05e681edd39c8d01fd6ee650 = $this->env->getExtension("native_profiler");
        $__internal_ab6b3197999686777ad7f253b026ead8fb87f87d05e681edd39c8d01fd6ee650->enter($__internal_ab6b3197999686777ad7f253b026ead8fb87f87d05e681edd39c8d01fd6ee650_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_ab6b3197999686777ad7f253b026ead8fb87f87d05e681edd39c8d01fd6ee650->leave($__internal_ab6b3197999686777ad7f253b026ead8fb87f87d05e681edd39c8d01fd6ee650_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
