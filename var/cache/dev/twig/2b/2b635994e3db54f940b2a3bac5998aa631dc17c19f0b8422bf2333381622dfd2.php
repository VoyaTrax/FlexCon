<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_f08e3c7612b05ed70292fe7010ed42bedcd11be76bf8e50746ceb1c3ebd3b79a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3603b2bf222485bc34fdeff94a86e7dd2093295ec24e9ce1aecf03ce0c37a83 = $this->env->getExtension("native_profiler");
        $__internal_e3603b2bf222485bc34fdeff94a86e7dd2093295ec24e9ce1aecf03ce0c37a83->enter($__internal_e3603b2bf222485bc34fdeff94a86e7dd2093295ec24e9ce1aecf03ce0c37a83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_e3603b2bf222485bc34fdeff94a86e7dd2093295ec24e9ce1aecf03ce0c37a83->leave($__internal_e3603b2bf222485bc34fdeff94a86e7dd2093295ec24e9ce1aecf03ce0c37a83_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
