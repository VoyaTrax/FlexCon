<?php

/* FlexConCmsBundle:Page:policy.html.twig */
class __TwigTemplate_597e2b39660624b4d1b79ae87cdd373c6ecdf7b4c5598e9d87fefc23fb6a24b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:policy.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99992b344db390bdc4152a7a3ca171d3a3cef8e548d31423b28e613a2ae5165a = $this->env->getExtension("native_profiler");
        $__internal_99992b344db390bdc4152a7a3ca171d3a3cef8e548d31423b28e613a2ae5165a->enter($__internal_99992b344db390bdc4152a7a3ca171d3a3cef8e548d31423b28e613a2ae5165a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:policy.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_99992b344db390bdc4152a7a3ca171d3a3cef8e548d31423b28e613a2ae5165a->leave($__internal_99992b344db390bdc4152a7a3ca171d3a3cef8e548d31423b28e613a2ae5165a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_58738a85b32da8cd07c8171014af7b37ab3270d049d27752bf0b20267fc92055 = $this->env->getExtension("native_profiler");
        $__internal_58738a85b32da8cd07c8171014af7b37ab3270d049d27752bf0b20267fc92055->enter($__internal_58738a85b32da8cd07c8171014af7b37ab3270d049d27752bf0b20267fc92055_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Privacy Policy";
        
        $__internal_58738a85b32da8cd07c8171014af7b37ab3270d049d27752bf0b20267fc92055->leave($__internal_58738a85b32da8cd07c8171014af7b37ab3270d049d27752bf0b20267fc92055_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_ff72e17daf280454f03daf1c29356bb32a383171a700fda9c98ba6718ad13dac = $this->env->getExtension("native_profiler");
        $__internal_ff72e17daf280454f03daf1c29356bb32a383171a700fda9c98ba6718ad13dac->enter($__internal_ff72e17daf280454f03daf1c29356bb32a383171a700fda9c98ba6718ad13dac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Privacy Policy</h1>
    </header>
    <article>
        <p>Privacy Policy info coming soon.</p>
    </article>
";
        
        $__internal_ff72e17daf280454f03daf1c29356bb32a383171a700fda9c98ba6718ad13dac->leave($__internal_ff72e17daf280454f03daf1c29356bb32a383171a700fda9c98ba6718ad13dac_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:policy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/policy.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Privacy Policy{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Privacy Policy</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Privacy Policy info coming soon.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
