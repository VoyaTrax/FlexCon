<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_bbda3d6822b6dafec6667594fc7c5e7ea3bf67c3b579042ed65e87d0b817abea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d4e184ab70690143b972e1680f227693dbf030582db9b00ff755a168bc81a172 = $this->env->getExtension("native_profiler");
        $__internal_d4e184ab70690143b972e1680f227693dbf030582db9b00ff755a168bc81a172->enter($__internal_d4e184ab70690143b972e1680f227693dbf030582db9b00ff755a168bc81a172_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d4e184ab70690143b972e1680f227693dbf030582db9b00ff755a168bc81a172->leave($__internal_d4e184ab70690143b972e1680f227693dbf030582db9b00ff755a168bc81a172_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
