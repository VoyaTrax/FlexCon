<?php

/* FlexConCmsBundle:Page:contact.html.twig */
class __TwigTemplate_77d764e2aee9940994e453923885af920f82a3a4d96c90b60b71c2dd98277940 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:contact.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cfeafd1d31135f288d7cd2b3cb17464195d7f10c712c44801349633fb4f3cae6 = $this->env->getExtension("native_profiler");
        $__internal_cfeafd1d31135f288d7cd2b3cb17464195d7f10c712c44801349633fb4f3cae6->enter($__internal_cfeafd1d31135f288d7cd2b3cb17464195d7f10c712c44801349633fb4f3cae6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cfeafd1d31135f288d7cd2b3cb17464195d7f10c712c44801349633fb4f3cae6->leave($__internal_cfeafd1d31135f288d7cd2b3cb17464195d7f10c712c44801349633fb4f3cae6_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_df32d9a2ae3315f8dc9e2b62f614042711301471794502cb955e765adfc220cd = $this->env->getExtension("native_profiler");
        $__internal_df32d9a2ae3315f8dc9e2b62f614042711301471794502cb955e765adfc220cd->enter($__internal_df32d9a2ae3315f8dc9e2b62f614042711301471794502cb955e765adfc220cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Contact";
        
        $__internal_df32d9a2ae3315f8dc9e2b62f614042711301471794502cb955e765adfc220cd->leave($__internal_df32d9a2ae3315f8dc9e2b62f614042711301471794502cb955e765adfc220cd_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_0fa5dfe536c89b363dd4878e1affc23a768fba721bd8586d748496dbad8af9f2 = $this->env->getExtension("native_profiler");
        $__internal_0fa5dfe536c89b363dd4878e1affc23a768fba721bd8586d748496dbad8af9f2->enter($__internal_0fa5dfe536c89b363dd4878e1affc23a768fba721bd8586d748496dbad8af9f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Contact Us</h1>
    </header>
    
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "blogger-notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 13
            echo "    <div class=\"blogger-notice\">
     {   { flashMessage }}
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "
    <p>
        Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla.
    </p>
    
    <form action=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_contact");
        echo "\" method=\"post\" class=\"blogger\">";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

        ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'row');
        echo "
        ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'row');
        echo "
        ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subject", array()), 'row');
        echo "
        ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "body", array()), 'row');
        echo "

        ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

        <input type=\"submit\" value=\"Submit\" />
    </form>
";
        
        $__internal_0fa5dfe536c89b363dd4878e1affc23a768fba721bd8586d748496dbad8af9f2->leave($__internal_0fa5dfe536c89b363dd4878e1affc23a768fba721bd8586d748496dbad8af9f2_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 34,  106 => 32,  102 => 31,  98 => 30,  94 => 29,  89 => 27,  83 => 26,  72 => 17,  63 => 13,  59 => 12,  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/contact.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Contact{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Contact Us</h1>*/
/*     </header>*/
/*     */
/*     {% for flashMessage in app.session.flashbag.get('blogger-notice') %}*/
/*     <div class="blogger-notice">*/
/*      {   { flashMessage }}*/
/*     </div>*/
/*     {% endfor %}*/
/* */
/*     <p>*/
/*         Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla.*/
/*     </p>*/
/*     */
/*     <form action="{{ path('flex_con_cms_contact') }}" method="post" class="blogger">{{ form_start(form) }}*/
/*         {{ form_errors(form) }}*/
/* */
/*         {{ form_row(form.name) }}*/
/*         {{ form_row(form.email) }}*/
/*         {{ form_row(form.subject) }}*/
/*         {{ form_row(form.body) }}*/
/* */
/*         {{ form_rest(form) }}*/
/* */
/*         <input type="submit" value="Submit" />*/
/*     </form>*/
/* {% endblock %}*/
/* */
