<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_a08e5bce67ad924b03d2d37472e5ce3cfc85c487821f43cd608345233df3c2bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aaa81ccd30d734660e456fa552d37347a8f899d9acf6174eebd1a21b03da81eb = $this->env->getExtension("native_profiler");
        $__internal_aaa81ccd30d734660e456fa552d37347a8f899d9acf6174eebd1a21b03da81eb->enter($__internal_aaa81ccd30d734660e456fa552d37347a8f899d9acf6174eebd1a21b03da81eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_aaa81ccd30d734660e456fa552d37347a8f899d9acf6174eebd1a21b03da81eb->leave($__internal_aaa81ccd30d734660e456fa552d37347a8f899d9acf6174eebd1a21b03da81eb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
