<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_21010544c9c3ce0dc78e22688ba44d18b4d07e516631091cddd0aab322345d10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ab8ac2f54bbb8b238a61444a4b89cd36b4015fb16f153727e43be548a4bf1fa = $this->env->getExtension("native_profiler");
        $__internal_9ab8ac2f54bbb8b238a61444a4b89cd36b4015fb16f153727e43be548a4bf1fa->enter($__internal_9ab8ac2f54bbb8b238a61444a4b89cd36b4015fb16f153727e43be548a4bf1fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_9ab8ac2f54bbb8b238a61444a4b89cd36b4015fb16f153727e43be548a4bf1fa->leave($__internal_9ab8ac2f54bbb8b238a61444a4b89cd36b4015fb16f153727e43be548a4bf1fa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
