<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_93305105936e3401f15556414f2d1e0ab2e22b1d20b9a3e60d81d7715ec36af6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72737c2362e3b72df5f7546cda404e4c3302c36a307f72e558a2f113e9ea9aa1 = $this->env->getExtension("native_profiler");
        $__internal_72737c2362e3b72df5f7546cda404e4c3302c36a307f72e558a2f113e9ea9aa1->enter($__internal_72737c2362e3b72df5f7546cda404e4c3302c36a307f72e558a2f113e9ea9aa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_72737c2362e3b72df5f7546cda404e4c3302c36a307f72e558a2f113e9ea9aa1->leave($__internal_72737c2362e3b72df5f7546cda404e4c3302c36a307f72e558a2f113e9ea9aa1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
