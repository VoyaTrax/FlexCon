<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_e007aa24b77050986717211c1cedbed1f71c430a2a8cc3a9b6971b530a0563bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c64cc917db56cb7af8821fcfa700c54ae15d62226800eb6eab5aa6b93283ed3 = $this->env->getExtension("native_profiler");
        $__internal_2c64cc917db56cb7af8821fcfa700c54ae15d62226800eb6eab5aa6b93283ed3->enter($__internal_2c64cc917db56cb7af8821fcfa700c54ae15d62226800eb6eab5aa6b93283ed3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2c64cc917db56cb7af8821fcfa700c54ae15d62226800eb6eab5aa6b93283ed3->leave($__internal_2c64cc917db56cb7af8821fcfa700c54ae15d62226800eb6eab5aa6b93283ed3_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_706adb147ddbfbf1952b46bd78bb3922ad5710f6c7774a63904b0ad58d501655 = $this->env->getExtension("native_profiler");
        $__internal_706adb147ddbfbf1952b46bd78bb3922ad5710f6c7774a63904b0ad58d501655->enter($__internal_706adb147ddbfbf1952b46bd78bb3922ad5710f6c7774a63904b0ad58d501655_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_706adb147ddbfbf1952b46bd78bb3922ad5710f6c7774a63904b0ad58d501655->leave($__internal_706adb147ddbfbf1952b46bd78bb3922ad5710f6c7774a63904b0ad58d501655_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7ddf1973cc110c18706de80a4fb07efdc8bccd2419f0be2b2ad4eca891b29aa6 = $this->env->getExtension("native_profiler");
        $__internal_7ddf1973cc110c18706de80a4fb07efdc8bccd2419f0be2b2ad4eca891b29aa6->enter($__internal_7ddf1973cc110c18706de80a4fb07efdc8bccd2419f0be2b2ad4eca891b29aa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_7ddf1973cc110c18706de80a4fb07efdc8bccd2419f0be2b2ad4eca891b29aa6->leave($__internal_7ddf1973cc110c18706de80a4fb07efdc8bccd2419f0be2b2ad4eca891b29aa6_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9de2f2c8f3cd4c3e88dc9754224620de88c6482cd661d1590210517e68de446f = $this->env->getExtension("native_profiler");
        $__internal_9de2f2c8f3cd4c3e88dc9754224620de88c6482cd661d1590210517e68de446f->enter($__internal_9de2f2c8f3cd4c3e88dc9754224620de88c6482cd661d1590210517e68de446f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_9de2f2c8f3cd4c3e88dc9754224620de88c6482cd661d1590210517e68de446f->leave($__internal_9de2f2c8f3cd4c3e88dc9754224620de88c6482cd661d1590210517e68de446f_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
