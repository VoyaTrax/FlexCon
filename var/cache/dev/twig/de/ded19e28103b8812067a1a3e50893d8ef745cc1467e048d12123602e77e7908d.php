<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_fc44584e092a5e1c7d1cef73b8b18695008e72266df32a7666f69fa3aa895761 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_799e370e836955876f5679d1796875ad37f1a2f53c45812e6f8efb9e4928e69b = $this->env->getExtension("native_profiler");
        $__internal_799e370e836955876f5679d1796875ad37f1a2f53c45812e6f8efb9e4928e69b->enter($__internal_799e370e836955876f5679d1796875ad37f1a2f53c45812e6f8efb9e4928e69b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_799e370e836955876f5679d1796875ad37f1a2f53c45812e6f8efb9e4928e69b->leave($__internal_799e370e836955876f5679d1796875ad37f1a2f53c45812e6f8efb9e4928e69b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
