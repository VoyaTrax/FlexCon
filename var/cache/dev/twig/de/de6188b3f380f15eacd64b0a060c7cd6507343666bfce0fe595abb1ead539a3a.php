<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_22b012b3e3daa2adbfe0c9d17d497b6b9532dce8799684765c1b6790d3863b91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a56e8adfff781ebbcff3fcf5ecfc2eb051df6550028be72f40dd3d84b94b02b3 = $this->env->getExtension("native_profiler");
        $__internal_a56e8adfff781ebbcff3fcf5ecfc2eb051df6550028be72f40dd3d84b94b02b3->enter($__internal_a56e8adfff781ebbcff3fcf5ecfc2eb051df6550028be72f40dd3d84b94b02b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_a56e8adfff781ebbcff3fcf5ecfc2eb051df6550028be72f40dd3d84b94b02b3->leave($__internal_a56e8adfff781ebbcff3fcf5ecfc2eb051df6550028be72f40dd3d84b94b02b3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
