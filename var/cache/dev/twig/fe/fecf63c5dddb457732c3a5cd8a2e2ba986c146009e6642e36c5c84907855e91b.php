<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_2801eca406f2ef8262cbda53ae1e822b2e9f59a255d6c067f0cd8305f22e2546 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ccf75478eab913510339f5595cb0dcd7920b9f594490583cb10f05b28781346 = $this->env->getExtension("native_profiler");
        $__internal_8ccf75478eab913510339f5595cb0dcd7920b9f594490583cb10f05b28781346->enter($__internal_8ccf75478eab913510339f5595cb0dcd7920b9f594490583cb10f05b28781346_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_8ccf75478eab913510339f5595cb0dcd7920b9f594490583cb10f05b28781346->leave($__internal_8ccf75478eab913510339f5595cb0dcd7920b9f594490583cb10f05b28781346_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
