<?php

/* FlexConCmsBundle:Page:contact.html.twig */
class __TwigTemplate_07902db4fcb62d2fa77e0eeec12077cd5ead885e232018d1b8c06d5bd9890eac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:contact.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3d73f802a81e1d4307d9876aa7ace6e74ee094c3758f21f5add7bc94755e71c = $this->env->getExtension("native_profiler");
        $__internal_b3d73f802a81e1d4307d9876aa7ace6e74ee094c3758f21f5add7bc94755e71c->enter($__internal_b3d73f802a81e1d4307d9876aa7ace6e74ee094c3758f21f5add7bc94755e71c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3d73f802a81e1d4307d9876aa7ace6e74ee094c3758f21f5add7bc94755e71c->leave($__internal_b3d73f802a81e1d4307d9876aa7ace6e74ee094c3758f21f5add7bc94755e71c_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_fe6bb31da0f1917a15816dfaece735865a154dda795fb3cb29921a6b0136161f = $this->env->getExtension("native_profiler");
        $__internal_fe6bb31da0f1917a15816dfaece735865a154dda795fb3cb29921a6b0136161f->enter($__internal_fe6bb31da0f1917a15816dfaece735865a154dda795fb3cb29921a6b0136161f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Contact";
        
        $__internal_fe6bb31da0f1917a15816dfaece735865a154dda795fb3cb29921a6b0136161f->leave($__internal_fe6bb31da0f1917a15816dfaece735865a154dda795fb3cb29921a6b0136161f_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_822ef0ecbe351606b2892edde56c5ff7e03bc373e6c957b632650d198e827630 = $this->env->getExtension("native_profiler");
        $__internal_822ef0ecbe351606b2892edde56c5ff7e03bc373e6c957b632650d198e827630->enter($__internal_822ef0ecbe351606b2892edde56c5ff7e03bc373e6c957b632650d198e827630_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Contact Us</h1>
    </header>
    <article>
        <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas
        condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis
        vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a
        lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
        posuere cubilia Curae.</p>
    </article>
";
        
        $__internal_822ef0ecbe351606b2892edde56c5ff7e03bc373e6c957b632650d198e827630->leave($__internal_822ef0ecbe351606b2892edde56c5ff7e03bc373e6c957b632650d198e827630_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/contact.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Contact{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Contact Us</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas*/
/*         condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis*/
/*         vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a*/
/*         lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices*/
/*         posuere cubilia Curae.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
