<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_6575e44d24944fd1709e52e537196a5997b2b425f08ef9425ab2f81f6461485f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf375577e8755d09e200444bef2bd219a2b4abd7aeefeb2a7b119f9798efece9 = $this->env->getExtension("native_profiler");
        $__internal_cf375577e8755d09e200444bef2bd219a2b4abd7aeefeb2a7b119f9798efece9->enter($__internal_cf375577e8755d09e200444bef2bd219a2b4abd7aeefeb2a7b119f9798efece9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_cf375577e8755d09e200444bef2bd219a2b4abd7aeefeb2a7b119f9798efece9->leave($__internal_cf375577e8755d09e200444bef2bd219a2b4abd7aeefeb2a7b119f9798efece9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
