<?php

/* FlexConCmsBundle:Comment:create.html.twig */
class __TwigTemplate_46406202c57054d3c1c9cd28d8ea8482a75e4a024d8a72f98c36c575859324a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Comment:create.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2feef4727294b72318ba1b90f6ae7fdbd8b06557d031fbeb73a4ae002a5daea4 = $this->env->getExtension("native_profiler");
        $__internal_2feef4727294b72318ba1b90f6ae7fdbd8b06557d031fbeb73a4ae002a5daea4->enter($__internal_2feef4727294b72318ba1b90f6ae7fdbd8b06557d031fbeb73a4ae002a5daea4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Comment:create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2feef4727294b72318ba1b90f6ae7fdbd8b06557d031fbeb73a4ae002a5daea4->leave($__internal_2feef4727294b72318ba1b90f6ae7fdbd8b06557d031fbeb73a4ae002a5daea4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_bbb4d1d576b2c9ec9638dfb20439addc996c448832612b25497f933580090ea8 = $this->env->getExtension("native_profiler");
        $__internal_bbb4d1d576b2c9ec9638dfb20439addc996c448832612b25497f933580090ea8->enter($__internal_bbb4d1d576b2c9ec9638dfb20439addc996c448832612b25497f933580090ea8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Add Comment";
        
        $__internal_bbb4d1d576b2c9ec9638dfb20439addc996c448832612b25497f933580090ea8->leave($__internal_bbb4d1d576b2c9ec9638dfb20439addc996c448832612b25497f933580090ea8_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_188cddb94cd21b43f8eea882302d02df024fea3ec767b2644f650b4a2834c63c = $this->env->getExtension("native_profiler");
        $__internal_188cddb94cd21b43f8eea882302d02df024fea3ec767b2644f650b4a2834c63c->enter($__internal_188cddb94cd21b43f8eea882302d02df024fea3ec767b2644f650b4a2834c63c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Add comment for blog post \"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "blog", array()), "title", array()), "html", null, true);
        echo "\"</h1>
    ";
        // line 7
        $this->loadTemplate("FlexConCmsBundle:Comment:form.html.twig", "FlexConCmsBundle:Comment:create.html.twig", 7)->display(array_merge($context, array("form" => (isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")))));
        
        $__internal_188cddb94cd21b43f8eea882302d02df024fea3ec767b2644f650b4a2834c63c->leave($__internal_188cddb94cd21b43f8eea882302d02df024fea3ec767b2644f650b4a2834c63c_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Comment:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* {% block title %}Add Comment{% endblock%}*/
/* */
/* {% block body %}*/
/*     <h1>Add comment for blog post "{{ comment.blog.title }}"</h1>*/
/*     {% include 'FlexConCmsBundle:Comment:form.html.twig' with { 'form': form } %}*/
/* {% endblock %}*/
/* */
