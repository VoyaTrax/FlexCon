<?php

/* FlexConCmsBundle:Page:about.html.twig */
class __TwigTemplate_a87b7337721400c3eb0ccf2976d6c7f29f1c4e807209c211790bf93bc8f948ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:about.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2694032855e999960cae6db7b3b7886c7bdc2c39865c66b55415d1bd62f65b1c = $this->env->getExtension("native_profiler");
        $__internal_2694032855e999960cae6db7b3b7886c7bdc2c39865c66b55415d1bd62f65b1c->enter($__internal_2694032855e999960cae6db7b3b7886c7bdc2c39865c66b55415d1bd62f65b1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:about.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2694032855e999960cae6db7b3b7886c7bdc2c39865c66b55415d1bd62f65b1c->leave($__internal_2694032855e999960cae6db7b3b7886c7bdc2c39865c66b55415d1bd62f65b1c_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_aae6b60e28dca3bef3e5a43909763973402e1a6bf81c9f3883b6ddb9868a701d = $this->env->getExtension("native_profiler");
        $__internal_aae6b60e28dca3bef3e5a43909763973402e1a6bf81c9f3883b6ddb9868a701d->enter($__internal_aae6b60e28dca3bef3e5a43909763973402e1a6bf81c9f3883b6ddb9868a701d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "About";
        
        $__internal_aae6b60e28dca3bef3e5a43909763973402e1a6bf81c9f3883b6ddb9868a701d->leave($__internal_aae6b60e28dca3bef3e5a43909763973402e1a6bf81c9f3883b6ddb9868a701d_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_a118d1ec9b87495346c8b3cd3eb138acfa43ab07047c521e8a826d3451e49235 = $this->env->getExtension("native_profiler");
        $__internal_a118d1ec9b87495346c8b3cd3eb138acfa43ab07047c521e8a826d3451e49235->enter($__internal_a118d1ec9b87495346c8b3cd3eb138acfa43ab07047c521e8a826d3451e49235_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>About Us</h1>
    </header>
    <article>
        <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas
        condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis
        vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a
        lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
        posuere cubilia Curae.</p>
    </article>
";
        
        $__internal_a118d1ec9b87495346c8b3cd3eb138acfa43ab07047c521e8a826d3451e49235->leave($__internal_a118d1ec9b87495346c8b3cd3eb138acfa43ab07047c521e8a826d3451e49235_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/about.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}About{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>About Us</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas*/
/*         condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis*/
/*         vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a*/
/*         lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices*/
/*         posuere cubilia Curae.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
