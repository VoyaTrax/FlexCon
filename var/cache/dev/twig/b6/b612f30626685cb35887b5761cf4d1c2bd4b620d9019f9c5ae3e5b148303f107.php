<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_3d6286fa12147578e50bc1314772d3d816751be1b974f611274f8e254a6b1f18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_517fee111b11415199077d51e6597dc0a6a8c833fc960fc0c564f04b7f8d29d7 = $this->env->getExtension("native_profiler");
        $__internal_517fee111b11415199077d51e6597dc0a6a8c833fc960fc0c564f04b7f8d29d7->enter($__internal_517fee111b11415199077d51e6597dc0a6a8c833fc960fc0c564f04b7f8d29d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_517fee111b11415199077d51e6597dc0a6a8c833fc960fc0c564f04b7f8d29d7->leave($__internal_517fee111b11415199077d51e6597dc0a6a8c833fc960fc0c564f04b7f8d29d7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
