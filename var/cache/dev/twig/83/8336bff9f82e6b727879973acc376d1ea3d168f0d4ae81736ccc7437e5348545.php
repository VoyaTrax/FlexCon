<?php

/* FlexConCmsBundle:Blog:show.html.twig */
class __TwigTemplate_acb7036df2d8df276b7c76a5c70bb3a63effac3a66f7a67c963ff0da68e09a8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Blog:show.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f9b5f51c7b6c2481091fcf2b5694f8bb0e55159055012ae74ecc522f7ebae51 = $this->env->getExtension("native_profiler");
        $__internal_5f9b5f51c7b6c2481091fcf2b5694f8bb0e55159055012ae74ecc522f7ebae51->enter($__internal_5f9b5f51c7b6c2481091fcf2b5694f8bb0e55159055012ae74ecc522f7ebae51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Blog:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5f9b5f51c7b6c2481091fcf2b5694f8bb0e55159055012ae74ecc522f7ebae51->leave($__internal_5f9b5f51c7b6c2481091fcf2b5694f8bb0e55159055012ae74ecc522f7ebae51_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_f93a29de7731afa22271cb85f7898404a94bfebbcb63f7c44e45ba067c1b66b6 = $this->env->getExtension("native_profiler");
        $__internal_f93a29de7731afa22271cb85f7898404a94bfebbcb63f7c44e45ba067c1b66b6->enter($__internal_f93a29de7731afa22271cb85f7898404a94bfebbcb63f7c44e45ba067c1b66b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "title", array()), "html", null, true);
        
        $__internal_f93a29de7731afa22271cb85f7898404a94bfebbcb63f7c44e45ba067c1b66b6->leave($__internal_f93a29de7731afa22271cb85f7898404a94bfebbcb63f7c44e45ba067c1b66b6_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_d925b1cec04cefa35a6aeefe6ca033bde16e7e0a89715be6f57894bba23cb587 = $this->env->getExtension("native_profiler");
        $__internal_d925b1cec04cefa35a6aeefe6ca033bde16e7e0a89715be6f57894bba23cb587->enter($__internal_d925b1cec04cefa35a6aeefe6ca033bde16e7e0a89715be6f57894bba23cb587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "    <article class=\"blog\">
        <header>
            <div class=\"date\"><time datetime=\"";
        // line 9
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "created", array()), "c"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "created", array()), "l, F j, Y"), "html", null, true);
        echo "</time></div>
            <h2>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "title", array()), "html", null, true);
        echo "</h2>
        </header>
        <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(twig_join_filter(array(0 => "images/", 1 => $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "image", array())))), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "title", array()), "html", null, true);
        echo " image not found\" class=\"large\" />
        <div>
            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "blog", array()), "html", null, true);
        echo "</p>
        </div>
    </article>
    
    
    <section class=\"comments\" id=\"comments\">
        <section class=\"previous-comments\">
            <h3>Comments</h3>
            ";
        // line 22
        $this->loadTemplate("FlexConCmsBundle:Comment:index.html.twig", "FlexConCmsBundle:Blog:show.html.twig", 22)->display(array_merge($context, array("comments" => (isset($context["comments"]) ? $context["comments"] : $this->getContext($context, "comments")))));
        // line 23
        echo "        </section>
        <h3>Add Comment</h3>
        ";
        // line 25
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FlexConCmsBundle:Comment:new", array("blog_id" => $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "id", array()))));
        echo "
    </section>
    
    
";
        
        $__internal_d925b1cec04cefa35a6aeefe6ca033bde16e7e0a89715be6f57894bba23cb587->leave($__internal_d925b1cec04cefa35a6aeefe6ca033bde16e7e0a89715be6f57894bba23cb587_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Blog:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 25,  88 => 23,  86 => 22,  75 => 14,  68 => 12,  63 => 10,  57 => 9,  53 => 7,  47 => 6,  35 => 4,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resouces/views/Blog/show.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* {% block title %}{{ blog.title }}{% endblock %}*/
/* */
/* {% block body %}*/
/*     <article class="blog">*/
/*         <header>*/
/*             <div class="date"><time datetime="{{ blog.created|date('c') }}">{{ blog.created|date('l, F j, Y') }}</time></div>*/
/*             <h2>{{ blog.title }}</h2>*/
/*         </header>*/
/*         <img src="{{ asset(['images/', blog.image]|join) }}" alt="{{ blog.title }} image not found" class="large" />*/
/*         <div>*/
/*             <p>{{ blog.blog }}</p>*/
/*         </div>*/
/*     </article>*/
/*     */
/*     */
/*     <section class="comments" id="comments">*/
/*         <section class="previous-comments">*/
/*             <h3>Comments</h3>*/
/*             {% include 'FlexConCmsBundle:Comment:index.html.twig' with { 'comments': comments } %}*/
/*         </section>*/
/*         <h3>Add Comment</h3>*/
/*         {{ render(controller( 'FlexConCmsBundle:Comment:new', { 'blog_id': blog.id } )) }}*/
/*     </section>*/
/*     */
/*     */
/* {% endblock %}*/
/* */
