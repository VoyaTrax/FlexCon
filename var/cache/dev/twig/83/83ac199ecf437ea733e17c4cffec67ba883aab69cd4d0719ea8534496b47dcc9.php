<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_714347cda5270a1822ff7885ef689d6e9431711934d3c4b28e673c072c8c09d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_324192187a69c24db4d867625ba62cb0cf53ec23dae61f6533c2092311bf0bb8 = $this->env->getExtension("native_profiler");
        $__internal_324192187a69c24db4d867625ba62cb0cf53ec23dae61f6533c2092311bf0bb8->enter($__internal_324192187a69c24db4d867625ba62cb0cf53ec23dae61f6533c2092311bf0bb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_324192187a69c24db4d867625ba62cb0cf53ec23dae61f6533c2092311bf0bb8->leave($__internal_324192187a69c24db4d867625ba62cb0cf53ec23dae61f6533c2092311bf0bb8_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_cba286b0a88932a7af5fb5bd98b0d8a0da4e05685d8f85f529d583b0422b5779 = $this->env->getExtension("native_profiler");
        $__internal_cba286b0a88932a7af5fb5bd98b0d8a0da4e05685d8f85f529d583b0422b5779->enter($__internal_cba286b0a88932a7af5fb5bd98b0d8a0da4e05685d8f85f529d583b0422b5779_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_cba286b0a88932a7af5fb5bd98b0d8a0da4e05685d8f85f529d583b0422b5779->leave($__internal_cba286b0a88932a7af5fb5bd98b0d8a0da4e05685d8f85f529d583b0422b5779_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_23bcf8a2054071d98e8d989c74d00e28aef0f200ea56de3c58783d371f5ff008 = $this->env->getExtension("native_profiler");
        $__internal_23bcf8a2054071d98e8d989c74d00e28aef0f200ea56de3c58783d371f5ff008->enter($__internal_23bcf8a2054071d98e8d989c74d00e28aef0f200ea56de3c58783d371f5ff008_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_23bcf8a2054071d98e8d989c74d00e28aef0f200ea56de3c58783d371f5ff008->leave($__internal_23bcf8a2054071d98e8d989c74d00e28aef0f200ea56de3c58783d371f5ff008_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_7fcaaedbdb878ef444e89cf688a707482861ef6e61812a89b4031e238f26c4d9 = $this->env->getExtension("native_profiler");
        $__internal_7fcaaedbdb878ef444e89cf688a707482861ef6e61812a89b4031e238f26c4d9->enter($__internal_7fcaaedbdb878ef444e89cf688a707482861ef6e61812a89b4031e238f26c4d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_7fcaaedbdb878ef444e89cf688a707482861ef6e61812a89b4031e238f26c4d9->leave($__internal_7fcaaedbdb878ef444e89cf688a707482861ef6e61812a89b4031e238f26c4d9_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
