<?php

/* FlexConCmsBundle:Page:contactEmail.txt.twig */
class __TwigTemplate_b5e9bd5901c807c727b610ed1257968d46f60e73f5ff394bd95697fd1ec26492 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_437be7dcba9f0a2fb1a26261ca0291690b15d256ab1525c034a87e97961fdeee = $this->env->getExtension("native_profiler");
        $__internal_437be7dcba9f0a2fb1a26261ca0291690b15d256ab1525c034a87e97961fdeee->enter($__internal_437be7dcba9f0a2fb1a26261ca0291690b15d256ab1525c034a87e97961fdeee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:contactEmail.txt.twig"));

        // line 2
        echo "A contact enquiry was made by ";
        echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : $this->getContext($context, "enquiry")), "name", array());
        echo " at ";
        echo twig_date_format_filter($this->env, "now", "Y-m-d H:i");
        echo ".

Reply-To: ";
        // line 4
        echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : $this->getContext($context, "enquiry")), "email", array());
        echo "
Subject: ";
        // line 5
        echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : $this->getContext($context, "enquiry")), "subject", array());
        echo "
Body:
";
        // line 7
        echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : $this->getContext($context, "enquiry")), "body", array());
        echo "
";
        
        $__internal_437be7dcba9f0a2fb1a26261ca0291690b15d256ab1525c034a87e97961fdeee->leave($__internal_437be7dcba9f0a2fb1a26261ca0291690b15d256ab1525c034a87e97961fdeee_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:contactEmail.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 7,  34 => 5,  30 => 4,  22 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/contactEmail.txt.twig #}*/
/* A contact enquiry was made by {{ enquiry.name }} at {{ "now" | date("Y-m-d H:i") }}.*/
/* */
/* Reply-To: {{ enquiry.email }}*/
/* Subject: {{ enquiry.subject }}*/
/* Body:*/
/* {{ enquiry.body }}*/
/* */
