<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_156a64ff5fa9819c5d341530b1bc4cdde0b878c4f951b2c59659d4d6ca9a84b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc48a31799b4ba7f5ae359f9e3ef500fa8a93ce8fa760a809443281b42916900 = $this->env->getExtension("native_profiler");
        $__internal_dc48a31799b4ba7f5ae359f9e3ef500fa8a93ce8fa760a809443281b42916900->enter($__internal_dc48a31799b4ba7f5ae359f9e3ef500fa8a93ce8fa760a809443281b42916900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_dc48a31799b4ba7f5ae359f9e3ef500fa8a93ce8fa760a809443281b42916900->leave($__internal_dc48a31799b4ba7f5ae359f9e3ef500fa8a93ce8fa760a809443281b42916900_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
