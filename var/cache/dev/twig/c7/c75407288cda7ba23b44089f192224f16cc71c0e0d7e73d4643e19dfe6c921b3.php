<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_5c2614c1d048b92dbd0ef1a9163a90b36081ee9a8d878acbb29f5ae462f91784 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd0d27699b7d9e53b8dd3349316c314b9fbccd99306383ada418072b6e584daa = $this->env->getExtension("native_profiler");
        $__internal_fd0d27699b7d9e53b8dd3349316c314b9fbccd99306383ada418072b6e584daa->enter($__internal_fd0d27699b7d9e53b8dd3349316c314b9fbccd99306383ada418072b6e584daa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_fd0d27699b7d9e53b8dd3349316c314b9fbccd99306383ada418072b6e584daa->leave($__internal_fd0d27699b7d9e53b8dd3349316c314b9fbccd99306383ada418072b6e584daa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
