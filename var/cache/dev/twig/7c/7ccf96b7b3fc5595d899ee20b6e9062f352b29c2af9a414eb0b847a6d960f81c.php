<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_0c43aa7334dddd6b8ece7fb1fe3bdb9cdf76c9b6fc71c5213b96de64aaeb93bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f2dd76c27f0c9397daa0eaa2cb452d84ad7a2a086e857b754af30dd6644264c = $this->env->getExtension("native_profiler");
        $__internal_8f2dd76c27f0c9397daa0eaa2cb452d84ad7a2a086e857b754af30dd6644264c->enter($__internal_8f2dd76c27f0c9397daa0eaa2cb452d84ad7a2a086e857b754af30dd6644264c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_8f2dd76c27f0c9397daa0eaa2cb452d84ad7a2a086e857b754af30dd6644264c->leave($__internal_8f2dd76c27f0c9397daa0eaa2cb452d84ad7a2a086e857b754af30dd6644264c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
