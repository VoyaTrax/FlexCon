<?php

/* FlexConCmsBundle:Page:imprint.html.twig */
class __TwigTemplate_6ba5a05dc279243369c5f96543cf4c0d4441d9aafb0e69a042125219acce80a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:imprint.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d64a30b9516d276600943fdd3d58d717a7df115ebed4e598832cb7a0f683c49f = $this->env->getExtension("native_profiler");
        $__internal_d64a30b9516d276600943fdd3d58d717a7df115ebed4e598832cb7a0f683c49f->enter($__internal_d64a30b9516d276600943fdd3d58d717a7df115ebed4e598832cb7a0f683c49f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:imprint.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d64a30b9516d276600943fdd3d58d717a7df115ebed4e598832cb7a0f683c49f->leave($__internal_d64a30b9516d276600943fdd3d58d717a7df115ebed4e598832cb7a0f683c49f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a1321d5649a3ebf1ef55a1f7965a0c5f37b6147f23327945818a7fc56ca141fb = $this->env->getExtension("native_profiler");
        $__internal_a1321d5649a3ebf1ef55a1f7965a0c5f37b6147f23327945818a7fc56ca141fb->enter($__internal_a1321d5649a3ebf1ef55a1f7965a0c5f37b6147f23327945818a7fc56ca141fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Imprint";
        
        $__internal_a1321d5649a3ebf1ef55a1f7965a0c5f37b6147f23327945818a7fc56ca141fb->leave($__internal_a1321d5649a3ebf1ef55a1f7965a0c5f37b6147f23327945818a7fc56ca141fb_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_231c1747428b5d1dcb125b6bd616a4d239aa8e73ea4834ccef8c6e7fdc23799f = $this->env->getExtension("native_profiler");
        $__internal_231c1747428b5d1dcb125b6bd616a4d239aa8e73ea4834ccef8c6e7fdc23799f->enter($__internal_231c1747428b5d1dcb125b6bd616a4d239aa8e73ea4834ccef8c6e7fdc23799f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Imprint</h1>
    </header>
    <article>
        <p>     
\t\t<h3>Site Owner</h3>
\t\t\tJohn Example<br>
\t\t\tExample Street. 1<br>
\t\t\t11111 Example City<br>
\t\t\tCountry<br>
\t\t<br><br>
\t\t<h3>Contact</h3>
\t\t\tTelephone: +00 666 666 666 66<br>
\t\t\tFax: +00 666 666 666 66<br>
\t\t<br>
\t\t\tE-Mail: <a href=\"mailto:john@example.com\">john@example.com</a><br>
\t\t<br>
\t\t\tWWW: <a href=\"http://www.example.com\" target=\"_blank\">www.example,com</a><br>
\t\t<br><br>
\t\t<h3>Content responsiblity</h3>
\t\t\tJohn Example<br>
\t\t\tExample Street. 1<br>
\t\t\t11111 Example City<br>
\t\t\tCountry<br>
\t\t<br><br>
\t\t<h3>Images and graphics source</h3>
\t\t\t<a href=\"http://www.example.com\" target=\"_blank\">www.example.com</a><br><br>
\t\t</p>
    </article>
";
        
        $__internal_231c1747428b5d1dcb125b6bd616a4d239aa8e73ea4834ccef8c6e7fdc23799f->leave($__internal_231c1747428b5d1dcb125b6bd616a4d239aa8e73ea4834ccef8c6e7fdc23799f_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:imprint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/imprint.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Imprint{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Imprint</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>     */
/* 		<h3>Site Owner</h3>*/
/* 			John Example<br>*/
/* 			Example Street. 1<br>*/
/* 			11111 Example City<br>*/
/* 			Country<br>*/
/* 		<br><br>*/
/* 		<h3>Contact</h3>*/
/* 			Telephone: +00 666 666 666 66<br>*/
/* 			Fax: +00 666 666 666 66<br>*/
/* 		<br>*/
/* 			E-Mail: <a href="mailto:john@example.com">john@example.com</a><br>*/
/* 		<br>*/
/* 			WWW: <a href="http://www.example.com" target="_blank">www.example,com</a><br>*/
/* 		<br><br>*/
/* 		<h3>Content responsiblity</h3>*/
/* 			John Example<br>*/
/* 			Example Street. 1<br>*/
/* 			11111 Example City<br>*/
/* 			Country<br>*/
/* 		<br><br>*/
/* 		<h3>Images and graphics source</h3>*/
/* 			<a href="http://www.example.com" target="_blank">www.example.com</a><br><br>*/
/* 		</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
