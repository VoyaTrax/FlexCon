<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_eaf4061d7fdd9c4cb95152e3b2a209c9a99b2344c9ca9dbdae4fbc8bc85cf448 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebcad3b587bc0e8efdc53fdf74393e3025a4add9636c0e9a7cc782a822ea5454 = $this->env->getExtension("native_profiler");
        $__internal_ebcad3b587bc0e8efdc53fdf74393e3025a4add9636c0e9a7cc782a822ea5454->enter($__internal_ebcad3b587bc0e8efdc53fdf74393e3025a4add9636c0e9a7cc782a822ea5454_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_ebcad3b587bc0e8efdc53fdf74393e3025a4add9636c0e9a7cc782a822ea5454->leave($__internal_ebcad3b587bc0e8efdc53fdf74393e3025a4add9636c0e9a7cc782a822ea5454_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
