<?php

/* FlexConCmsBundle:Page:imprint.html.twig */
class __TwigTemplate_2004eabaecee168e42f4894968d6171108b4a124279e03a7811ec71694882d6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:imprint.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d663a765343aa18164bd5d605cb925b871a96f97c946215fbd11ca2c08b78c3 = $this->env->getExtension("native_profiler");
        $__internal_9d663a765343aa18164bd5d605cb925b871a96f97c946215fbd11ca2c08b78c3->enter($__internal_9d663a765343aa18164bd5d605cb925b871a96f97c946215fbd11ca2c08b78c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:imprint.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9d663a765343aa18164bd5d605cb925b871a96f97c946215fbd11ca2c08b78c3->leave($__internal_9d663a765343aa18164bd5d605cb925b871a96f97c946215fbd11ca2c08b78c3_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_425a9c4b8865ed00bee1c98d2b2c90f8a29daffeefc5ffd5fb204081d45c569f = $this->env->getExtension("native_profiler");
        $__internal_425a9c4b8865ed00bee1c98d2b2c90f8a29daffeefc5ffd5fb204081d45c569f->enter($__internal_425a9c4b8865ed00bee1c98d2b2c90f8a29daffeefc5ffd5fb204081d45c569f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Imprint";
        
        $__internal_425a9c4b8865ed00bee1c98d2b2c90f8a29daffeefc5ffd5fb204081d45c569f->leave($__internal_425a9c4b8865ed00bee1c98d2b2c90f8a29daffeefc5ffd5fb204081d45c569f_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_013445deed3e2b81f3dac84adfa10cdebd7e7bbc842cdca3756957783fff7452 = $this->env->getExtension("native_profiler");
        $__internal_013445deed3e2b81f3dac84adfa10cdebd7e7bbc842cdca3756957783fff7452->enter($__internal_013445deed3e2b81f3dac84adfa10cdebd7e7bbc842cdca3756957783fff7452_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>Imprint</h1>
    </header>
    <article>
        <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas
        condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis
        vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a
        lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
        posuere cubilia Curae.</p>
    </article>
";
        
        $__internal_013445deed3e2b81f3dac84adfa10cdebd7e7bbc842cdca3756957783fff7452->leave($__internal_013445deed3e2b81f3dac84adfa10cdebd7e7bbc842cdca3756957783fff7452_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:imprint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/imprint.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}Imprint{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>Imprint</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas*/
/*         condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis*/
/*         vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a*/
/*         lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices*/
/*         posuere cubilia Curae.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
