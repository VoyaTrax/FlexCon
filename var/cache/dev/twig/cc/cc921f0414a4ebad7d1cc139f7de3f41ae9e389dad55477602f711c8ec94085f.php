<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_8ffb3a010b0b649525b13ef9f36172401951b6fbf7afaa75b7496ab71e8ea40e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4f42c331ff822bfda153d68c5231db06600faac98f8076e87a49e534f0b1890 = $this->env->getExtension("native_profiler");
        $__internal_c4f42c331ff822bfda153d68c5231db06600faac98f8076e87a49e534f0b1890->enter($__internal_c4f42c331ff822bfda153d68c5231db06600faac98f8076e87a49e534f0b1890_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_c4f42c331ff822bfda153d68c5231db06600faac98f8076e87a49e534f0b1890->leave($__internal_c4f42c331ff822bfda153d68c5231db06600faac98f8076e87a49e534f0b1890_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
