<?php

/* ::base.html.twig */
class __TwigTemplate_886efbb57439487dcf08995b2ae81c3769acd17132de9f08426080a4988c0548 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'blog_title' => array($this, 'block_blog_title'),
            'blog_tagline' => array($this, 'block_blog_tagline'),
            'body' => array($this, 'block_body'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb68242dc820fcde7ca05db095ab93e14f8b756f015cd26e672d343a99718f81 = $this->env->getExtension("native_profiler");
        $__internal_cb68242dc820fcde7ca05db095ab93e14f8b756f015cd26e672d343a99718f81->enter($__internal_cb68242dc820fcde7ca05db095ab93e14f8b756f015cd26e672d343a99718f81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!-- app/Resources/views/base.html.twig -->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html\"; charset=utf-8\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo " - FlexCon</title>
        <!--[if lt IE 9]>
            <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
        <![endif]-->
        ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "        <link rel=\"shortcut icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        <section id=\"wrapper\">
            <header id=\"header\">
                <div class=\"top\">
                    ";
        // line 22
        $this->displayBlock('navigation', $context, $blocks);
        // line 31
        echo "                </div>

                <hgroup>
                    <h2>";
        // line 34
        $this->displayBlock('blog_title', $context, $blocks);
        echo "</h2>
                    <h3>";
        // line 35
        $this->displayBlock('blog_tagline', $context, $blocks);
        echo "</h3>
                </hgroup>
            </header>

            <section class=\"main-col\">
                ";
        // line 40
        $this->displayBlock('body', $context, $blocks);
        // line 41
        echo "            </section>
            <aside class=\"sidebar\">
                ";
        // line 43
        $this->displayBlock('sidebar', $context, $blocks);
        // line 44
        echo "            </aside>

            <div id=\"footer\">
                ";
        // line 47
        $this->displayBlock('footer', $context, $blocks);
        // line 51
        echo "            </div>
        </section>

        ";
        // line 54
        $this->displayBlock('javascripts', $context, $blocks);
        // line 55
        echo "    </body>
</html>
";
        
        $__internal_cb68242dc820fcde7ca05db095ab93e14f8b756f015cd26e672d343a99718f81->leave($__internal_cb68242dc820fcde7ca05db095ab93e14f8b756f015cd26e672d343a99718f81_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_a5261c51b2cb16d6f4e57b6d184c8d0821f0a17e766ad71d1cba1ffc1a386a91 = $this->env->getExtension("native_profiler");
        $__internal_a5261c51b2cb16d6f4e57b6d184c8d0821f0a17e766ad71d1cba1ffc1a386a91->enter($__internal_a5261c51b2cb16d6f4e57b6d184c8d0821f0a17e766ad71d1cba1ffc1a386a91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "VoyaTrax Project";
        
        $__internal_a5261c51b2cb16d6f4e57b6d184c8d0821f0a17e766ad71d1cba1ffc1a386a91->leave($__internal_a5261c51b2cb16d6f4e57b6d184c8d0821f0a17e766ad71d1cba1ffc1a386a91_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1085dbed69ceac20cb1f8f86bc75a8e098f6a84d73337cafd04aa96ccf7a28dc = $this->env->getExtension("native_profiler");
        $__internal_1085dbed69ceac20cb1f8f86bc75a8e098f6a84d73337cafd04aa96ccf7a28dc->enter($__internal_1085dbed69ceac20cb1f8f86bc75a8e098f6a84d73337cafd04aa96ccf7a28dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "            <link href='http://fonts.googleapis.com/css?family=Irish+Grover' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=La+Belle+Aurore' rel='stylesheet' type='text/css'>
            <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/screen.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
        ";
        
        $__internal_1085dbed69ceac20cb1f8f86bc75a8e098f6a84d73337cafd04aa96ccf7a28dc->leave($__internal_1085dbed69ceac20cb1f8f86bc75a8e098f6a84d73337cafd04aa96ccf7a28dc_prof);

    }

    // line 22
    public function block_navigation($context, array $blocks = array())
    {
        $__internal_4f6dbe092118fe1f849b1f063357a335725d46428c9f36b40a0496164ccbd7ff = $this->env->getExtension("native_profiler");
        $__internal_4f6dbe092118fe1f849b1f063357a335725d46428c9f36b40a0496164ccbd7ff->enter($__internal_4f6dbe092118fe1f849b1f063357a335725d46428c9f36b40a0496164ccbd7ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navigation"));

        // line 23
        echo "                        <nav>
                            <ul class=\"navigation\">
                                <li><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_homepage");
        echo "\">Home</a></li>
                                <li><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_about");
        echo "\">About</a></li>
                                <li><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_contact");
        echo "\">Contact</a></li>
                            </ul>
                        </nav>
                    ";
        
        $__internal_4f6dbe092118fe1f849b1f063357a335725d46428c9f36b40a0496164ccbd7ff->leave($__internal_4f6dbe092118fe1f849b1f063357a335725d46428c9f36b40a0496164ccbd7ff_prof);

    }

    // line 34
    public function block_blog_title($context, array $blocks = array())
    {
        $__internal_bc37cb6bb2d8c499767310c6493cad8f70e25509dc0e3345c90b342792b94f13 = $this->env->getExtension("native_profiler");
        $__internal_bc37cb6bb2d8c499767310c6493cad8f70e25509dc0e3345c90b342792b94f13->enter($__internal_bc37cb6bb2d8c499767310c6493cad8f70e25509dc0e3345c90b342792b94f13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "blog_title"));

        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_homepage");
        echo "\">FlexCon</a>";
        
        $__internal_bc37cb6bb2d8c499767310c6493cad8f70e25509dc0e3345c90b342792b94f13->leave($__internal_bc37cb6bb2d8c499767310c6493cad8f70e25509dc0e3345c90b342792b94f13_prof);

    }

    // line 35
    public function block_blog_tagline($context, array $blocks = array())
    {
        $__internal_4f849bb1f0bdfa540532dcdc8769b37723ec9775341b56d3a255f5292d7db3d4 = $this->env->getExtension("native_profiler");
        $__internal_4f849bb1f0bdfa540532dcdc8769b37723ec9775341b56d3a255f5292d7db3d4->enter($__internal_4f849bb1f0bdfa540532dcdc8769b37723ec9775341b56d3a255f5292d7db3d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "blog_tagline"));

        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_homepage");
        echo "\">FUN | FAST | SECURE'</a>";
        
        $__internal_4f849bb1f0bdfa540532dcdc8769b37723ec9775341b56d3a255f5292d7db3d4->leave($__internal_4f849bb1f0bdfa540532dcdc8769b37723ec9775341b56d3a255f5292d7db3d4_prof);

    }

    // line 40
    public function block_body($context, array $blocks = array())
    {
        $__internal_02ac29d653387249e4f1d0ed0613e7a995306ddab2372eace9f45b1357c1ce8a = $this->env->getExtension("native_profiler");
        $__internal_02ac29d653387249e4f1d0ed0613e7a995306ddab2372eace9f45b1357c1ce8a->enter($__internal_02ac29d653387249e4f1d0ed0613e7a995306ddab2372eace9f45b1357c1ce8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_02ac29d653387249e4f1d0ed0613e7a995306ddab2372eace9f45b1357c1ce8a->leave($__internal_02ac29d653387249e4f1d0ed0613e7a995306ddab2372eace9f45b1357c1ce8a_prof);

    }

    // line 43
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_34ab2d452e84e348eaca3a6834d81ca030337af7ac837d8197343ae2240f5970 = $this->env->getExtension("native_profiler");
        $__internal_34ab2d452e84e348eaca3a6834d81ca030337af7ac837d8197343ae2240f5970->enter($__internal_34ab2d452e84e348eaca3a6834d81ca030337af7ac837d8197343ae2240f5970_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        
        $__internal_34ab2d452e84e348eaca3a6834d81ca030337af7ac837d8197343ae2240f5970->leave($__internal_34ab2d452e84e348eaca3a6834d81ca030337af7ac837d8197343ae2240f5970_prof);

    }

    // line 47
    public function block_footer($context, array $blocks = array())
    {
        $__internal_55e2373a1f1f6cec3c638a029edef8bd42d4cb505d32a583d8032f5d5733f95f = $this->env->getExtension("native_profiler");
        $__internal_55e2373a1f1f6cec3c638a029edef8bd42d4cb505d32a583d8032f5d5733f95f->enter($__internal_55e2373a1f1f6cec3c638a029edef8bd42d4cb505d32a583d8032f5d5733f95f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 48
        echo "                    FlexCon &copy; ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " <a href=\"http://jmcoleman.eu\" target=\"_blank\">JMColeman</a> for the <a href=\"http://voyatrax.org\" target=\"_blank\">VoyaTrax Project</a><br> <font size=\"-2\">Based on Symfony2 blog tutorial - created by <a href=\"https://github.com/dsyph3r\" target=\"_blank\">dsyph3r</a></font><br><br>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_terms");
        echo "\">Terms of Use</a> | <a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_policy");
        echo "\">Privacy Policy</a> | <a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_license");
        echo "\">License(s)</a> | <a href=\"";
        echo $this->env->getExtension('routing')->getPath("flex_con_cms_imprint");
        echo "\">Imprint</a>
                ";
        
        $__internal_55e2373a1f1f6cec3c638a029edef8bd42d4cb505d32a583d8032f5d5733f95f->leave($__internal_55e2373a1f1f6cec3c638a029edef8bd42d4cb505d32a583d8032f5d5733f95f_prof);

    }

    // line 54
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_cd7c204959d57d5ec1ad963a2e12ef8a47388fcd3a6cfe22529896b338359a25 = $this->env->getExtension("native_profiler");
        $__internal_cd7c204959d57d5ec1ad963a2e12ef8a47388fcd3a6cfe22529896b338359a25->enter($__internal_cd7c204959d57d5ec1ad963a2e12ef8a47388fcd3a6cfe22529896b338359a25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_cd7c204959d57d5ec1ad963a2e12ef8a47388fcd3a6cfe22529896b338359a25->leave($__internal_cd7c204959d57d5ec1ad963a2e12ef8a47388fcd3a6cfe22529896b338359a25_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 54,  229 => 49,  224 => 48,  218 => 47,  207 => 43,  196 => 40,  182 => 35,  168 => 34,  157 => 27,  153 => 26,  149 => 25,  145 => 23,  139 => 22,  130 => 13,  126 => 11,  120 => 10,  108 => 6,  99 => 55,  97 => 54,  92 => 51,  90 => 47,  85 => 44,  83 => 43,  79 => 41,  77 => 40,  69 => 35,  65 => 34,  60 => 31,  58 => 22,  47 => 15,  45 => 10,  38 => 6,  31 => 1,);
    }
}
/* <!-- app/Resources/views/base.html.twig -->*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta http-equiv="Content-Type" content="text/html"; charset=utf-8" />*/
/*         <title>{% block title %}VoyaTrax Project{% endblock %} - FlexCon</title>*/
/*         <!--[if lt IE 9]>*/
/*             <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/*         <![endif]-->*/
/*         {% block stylesheets %}*/
/*             <link href='http://fonts.googleapis.com/css?family=Irish+Grover' rel='stylesheet' type='text/css'>*/
/*             <link href='http://fonts.googleapis.com/css?family=La+Belle+Aurore' rel='stylesheet' type='text/css'>*/
/*             <link href="{{ asset('css/screen.css') }}" type="text/css" rel="stylesheet" />*/
/*         {% endblock %}*/
/*         <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/* */
/*         <section id="wrapper">*/
/*             <header id="header">*/
/*                 <div class="top">*/
/*                     {% block navigation %}*/
/*                         <nav>*/
/*                             <ul class="navigation">*/
/*                                 <li><a href="{{ path('flex_con_cms_homepage') }}">Home</a></li>*/
/*                                 <li><a href="{{ path('flex_con_cms_about') }}">About</a></li>*/
/*                                 <li><a href="{{ path('flex_con_cms_contact') }}">Contact</a></li>*/
/*                             </ul>*/
/*                         </nav>*/
/*                     {% endblock %}*/
/*                 </div>*/
/* */
/*                 <hgroup>*/
/*                     <h2>{% block blog_title %}<a href="{{ path('flex_con_cms_homepage') }}">FlexCon</a>{% endblock %}</h2>*/
/*                     <h3>{% block blog_tagline %}<a href="{{ path('flex_con_cms_homepage') }}">FUN | FAST | SECURE'</a>{% endblock %}</h3>*/
/*                 </hgroup>*/
/*             </header>*/
/* */
/*             <section class="main-col">*/
/*                 {% block body %}{% endblock %}*/
/*             </section>*/
/*             <aside class="sidebar">*/
/*                 {% block sidebar %}{% endblock %}*/
/*             </aside>*/
/* */
/*             <div id="footer">*/
/*                 {% block footer %}*/
/*                     FlexCon &copy; {{ "now"|date ("Y") }} <a href="http://jmcoleman.eu" target="_blank">JMColeman</a> for the <a href="http://voyatrax.org" target="_blank">VoyaTrax Project</a><br> <font size="-2">Based on Symfony2 blog tutorial - created by <a href="https://github.com/dsyph3r" target="_blank">dsyph3r</a></font><br><br>*/
/*                     <a href="{{ path('flex_con_cms_terms') }}">Terms of Use</a> | <a href="{{ path('flex_con_cms_policy') }}">Privacy Policy</a> | <a href="{{ path('flex_con_cms_license') }}">License(s)</a> | <a href="{{ path('flex_con_cms_imprint') }}">Imprint</a>*/
/*                 {% endblock %}*/
/*             </div>*/
/*         </section>*/
/* */
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
