<?php

/* FlexConCmsBundle:Comment:form.html.twig */
class __TwigTemplate_9a4741bcc32abb73edf5e0ab746ce1e98f233a4bc4c841a9d5d5b17a74e67864 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8292e99d67018ca28fbf0b262de8f8af52c59bf93a23ae9a0d7df7c65a6846a9 = $this->env->getExtension("native_profiler");
        $__internal_8292e99d67018ca28fbf0b262de8f8af52c59bf93a23ae9a0d7df7c65a6846a9->enter($__internal_8292e99d67018ca28fbf0b262de8f8af52c59bf93a23ae9a0d7df7c65a6846a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Comment:form.html.twig"));

        // line 2
        echo "
<form action=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("flex_con_cms_comment_create", array("blog_id" => $this->getAttribute($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "blog", array()), "id", array()))), "html", null, true);
        echo "\" method=\"post\" class=\"blogger\">";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <p>
        <input type=\"submit\" value=\"Submit\">
    </p>
</form>
";
        
        $__internal_8292e99d67018ca28fbf0b262de8f8af52c59bf93a23ae9a0d7df7c65a6846a9->leave($__internal_8292e99d67018ca28fbf0b262de8f8af52c59bf93a23ae9a0d7df7c65a6846a9_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Comment:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  25 => 3,  22 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Comment/form.html.twig #}*/
/* */
/* <form action="{{ path('flex_con_cms_comment_create', { 'blog_id' : comment.blog.id } ) }}" method="post" class="blogger">{{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     <p>*/
/*         <input type="submit" value="Submit">*/
/*     </p>*/
/* </form>*/
/* */
