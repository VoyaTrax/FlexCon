<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_b518cdd7ad444453bc15580b775c82fe09d16d0cdd8adf679f948304e11eec75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac84625d3bceed58d3ed474b9a2183529ac7deb4653e7fbd54d1f3b5d3606711 = $this->env->getExtension("native_profiler");
        $__internal_ac84625d3bceed58d3ed474b9a2183529ac7deb4653e7fbd54d1f3b5d3606711->enter($__internal_ac84625d3bceed58d3ed474b9a2183529ac7deb4653e7fbd54d1f3b5d3606711_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_ac84625d3bceed58d3ed474b9a2183529ac7deb4653e7fbd54d1f3b5d3606711->leave($__internal_ac84625d3bceed58d3ed474b9a2183529ac7deb4653e7fbd54d1f3b5d3606711_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
