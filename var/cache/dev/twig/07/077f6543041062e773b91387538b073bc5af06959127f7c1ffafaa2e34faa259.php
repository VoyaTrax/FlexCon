<?php

/* FlexConCmsBundle:Page:about.html.twig */
class __TwigTemplate_7d5f3d42b7b5395196a53c84ef9c0999534006e19eec4da286d59068db2efe01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:about.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_505db3b0d328656a7b1ea1827bc7caad82ad2a986f91bfa63ce0b63bc09c3cc2 = $this->env->getExtension("native_profiler");
        $__internal_505db3b0d328656a7b1ea1827bc7caad82ad2a986f91bfa63ce0b63bc09c3cc2->enter($__internal_505db3b0d328656a7b1ea1827bc7caad82ad2a986f91bfa63ce0b63bc09c3cc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:about.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_505db3b0d328656a7b1ea1827bc7caad82ad2a986f91bfa63ce0b63bc09c3cc2->leave($__internal_505db3b0d328656a7b1ea1827bc7caad82ad2a986f91bfa63ce0b63bc09c3cc2_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_55e37eb27156e2d60218a03cd594cd48e6de6f46eee3bce0fac609ebfc1eb545 = $this->env->getExtension("native_profiler");
        $__internal_55e37eb27156e2d60218a03cd594cd48e6de6f46eee3bce0fac609ebfc1eb545->enter($__internal_55e37eb27156e2d60218a03cd594cd48e6de6f46eee3bce0fac609ebfc1eb545_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "About";
        
        $__internal_55e37eb27156e2d60218a03cd594cd48e6de6f46eee3bce0fac609ebfc1eb545->leave($__internal_55e37eb27156e2d60218a03cd594cd48e6de6f46eee3bce0fac609ebfc1eb545_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_f59cd6662edbe711877852760704a599b781c53e76f1e70b445f0ca0a20b3587 = $this->env->getExtension("native_profiler");
        $__internal_f59cd6662edbe711877852760704a599b781c53e76f1e70b445f0ca0a20b3587->enter($__internal_f59cd6662edbe711877852760704a599b781c53e76f1e70b445f0ca0a20b3587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <header>
        <h1>About Us</h1>
    </header>
    <article>
        <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit
        amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.
        Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue
        urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida
        tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas
        condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis
        vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a
        lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
        posuere cubilia Curae.</p>
    </article>
";
        
        $__internal_f59cd6662edbe711877852760704a599b781c53e76f1e70b445f0ca0a20b3587->leave($__internal_f59cd6662edbe711877852760704a599b781c53e76f1e70b445f0ca0a20b3587_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  47 => 7,  35 => 5,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/about.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* */
/* {% block title %}About{% endblock%}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <h1>About Us</h1>*/
/*     </header>*/
/*     <article>*/
/*         <p>Donec imperdiet ante sed diam consequat et dictum erat faucibus. Aliquam sit*/
/*         amet vehicula leo. Morbi urna dui, tempor ac posuere et, rutrum at dui.*/
/*         Curabitur neque quam, ultricies ut imperdiet id, ornare varius arcu. Ut congue*/
/*         urna sit amet tellus malesuada nec elementum risus molestie. Donec gravida*/
/*         tellus sed tortor adipiscing fringilla. Donec nulla mauris, mollis egestas*/
/*         condimentum laoreet, lacinia vel lorem. Morbi vitae justo sit amet felis*/
/*         vehicula commodo a placerat lacus. Mauris at est elit, nec vehicula urna. Duis a*/
/*         lacus nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices*/
/*         posuere cubilia Curae.</p>*/
/*     </article>*/
/* {% endblock %}*/
/* */
