<?php

/* FlexConCmsBundle:Page:index.html.twig */
class __TwigTemplate_c5be49db8aa03a095368779bad85473c77c5ff145acb8f3ea38d8550bc56bda0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FlexConCmsBundle::layout.html.twig", "FlexConCmsBundle:Page:index.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d8736c3c6aade23450befc55b335c1352eea7164b5891809c9ac9eaae605b67 = $this->env->getExtension("native_profiler");
        $__internal_3d8736c3c6aade23450befc55b335c1352eea7164b5891809c9ac9eaae605b67->enter($__internal_3d8736c3c6aade23450befc55b335c1352eea7164b5891809c9ac9eaae605b67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle:Page:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3d8736c3c6aade23450befc55b335c1352eea7164b5891809c9ac9eaae605b67->leave($__internal_3d8736c3c6aade23450befc55b335c1352eea7164b5891809c9ac9eaae605b67_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_4cb46c5f26fbbe861827e07000686c041318356e75a0f5860104720f7e0fb687 = $this->env->getExtension("native_profiler");
        $__internal_4cb46c5f26fbbe861827e07000686c041318356e75a0f5860104720f7e0fb687->enter($__internal_4cb46c5f26fbbe861827e07000686c041318356e75a0f5860104720f7e0fb687_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    Blog homepage
";
        
        $__internal_4cb46c5f26fbbe861827e07000686c041318356e75a0f5860104720f7e0fb687->leave($__internal_4cb46c5f26fbbe861827e07000686c041318356e75a0f5860104720f7e0fb687_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle:Page:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 5,  34 => 4,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/Page/index.html.twig #}*/
/* {% extends 'FlexConCmsBundle::layout.html.twig' %}*/
/* */
/* {% block body %}*/
/*     Blog homepage*/
/* {% endblock %}*/
/* */
