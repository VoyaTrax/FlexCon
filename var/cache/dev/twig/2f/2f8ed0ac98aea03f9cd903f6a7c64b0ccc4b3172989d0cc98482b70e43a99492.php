<?php

/* FlexConCmsBundle::layout.html.twig */
class __TwigTemplate_ea865ff41c13231047a0efcdbaf38f5657a2bd12d53768338eff99ab74698936 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "FlexConCmsBundle::layout.html.twig", 2);
        $this->blocks = array(
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b39095a45c32e1dd5e0ccec606b6cd060c36402af4b108a770e39f8eaa2066cc = $this->env->getExtension("native_profiler");
        $__internal_b39095a45c32e1dd5e0ccec606b6cd060c36402af4b108a770e39f8eaa2066cc->enter($__internal_b39095a45c32e1dd5e0ccec606b6cd060c36402af4b108a770e39f8eaa2066cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FlexConCmsBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b39095a45c32e1dd5e0ccec606b6cd060c36402af4b108a770e39f8eaa2066cc->leave($__internal_b39095a45c32e1dd5e0ccec606b6cd060c36402af4b108a770e39f8eaa2066cc_prof);

    }

    // line 4
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_3a040369223c0fd08a77cba56eaef644ce8f6e457355a96b6c426c2acaceae3a = $this->env->getExtension("native_profiler");
        $__internal_3a040369223c0fd08a77cba56eaef644ce8f6e457355a96b6c426c2acaceae3a->enter($__internal_3a040369223c0fd08a77cba56eaef644ce8f6e457355a96b6c426c2acaceae3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 5
        echo "    Sidebar content
";
        
        $__internal_3a040369223c0fd08a77cba56eaef644ce8f6e457355a96b6c426c2acaceae3a->leave($__internal_3a040369223c0fd08a77cba56eaef644ce8f6e457355a96b6c426c2acaceae3a_prof);

    }

    public function getTemplateName()
    {
        return "FlexConCmsBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 5,  34 => 4,  11 => 2,);
    }
}
/* {# src/FlexCon/CmsBundle/Resources/views/layout.html.twig #}*/
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block sidebar %}*/
/*     Sidebar content*/
/* {% endblock %}*/
/* */
