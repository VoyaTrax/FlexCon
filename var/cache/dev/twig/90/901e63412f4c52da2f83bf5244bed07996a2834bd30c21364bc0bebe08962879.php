<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_8c952948f9efd4ed0cc76c03ba3bb3582197900c733e62f8ae6c2591d3b8d3aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e588f4354912f3fdd143aaa86185f861661fddc5f8783758d71415dfe5d10176 = $this->env->getExtension("native_profiler");
        $__internal_e588f4354912f3fdd143aaa86185f861661fddc5f8783758d71415dfe5d10176->enter($__internal_e588f4354912f3fdd143aaa86185f861661fddc5f8783758d71415dfe5d10176_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_e588f4354912f3fdd143aaa86185f861661fddc5f8783758d71415dfe5d10176->leave($__internal_e588f4354912f3fdd143aaa86185f861661fddc5f8783758d71415dfe5d10176_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
