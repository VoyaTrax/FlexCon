<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_d5757f651e2d4eed287e5ac08e07cbd4ea83ecc28d58f191569c2b2770287144 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ddbf32418b5634224ec5748c3b696c952ede9432f796a1edfcc82b5d1c18271d = $this->env->getExtension("native_profiler");
        $__internal_ddbf32418b5634224ec5748c3b696c952ede9432f796a1edfcc82b5d1c18271d->enter($__internal_ddbf32418b5634224ec5748c3b696c952ede9432f796a1edfcc82b5d1c18271d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_ddbf32418b5634224ec5748c3b696c952ede9432f796a1edfcc82b5d1c18271d->leave($__internal_ddbf32418b5634224ec5748c3b696c952ede9432f796a1edfcc82b5d1c18271d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
