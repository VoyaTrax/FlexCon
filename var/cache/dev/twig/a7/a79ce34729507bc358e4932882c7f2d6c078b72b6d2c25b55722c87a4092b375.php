<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_d08125ec668035c6b10d1927606da1c11c60048375f0bd8fcaa475a7291bd6a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56e92d7a6b3051e89e39f32803513f8422630f14ad69c1aa448d3423ee482195 = $this->env->getExtension("native_profiler");
        $__internal_56e92d7a6b3051e89e39f32803513f8422630f14ad69c1aa448d3423ee482195->enter($__internal_56e92d7a6b3051e89e39f32803513f8422630f14ad69c1aa448d3423ee482195_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_56e92d7a6b3051e89e39f32803513f8422630f14ad69c1aa448d3423ee482195->leave($__internal_56e92d7a6b3051e89e39f32803513f8422630f14ad69c1aa448d3423ee482195_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
