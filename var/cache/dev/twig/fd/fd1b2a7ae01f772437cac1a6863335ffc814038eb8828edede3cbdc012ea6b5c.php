<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_e2300b862c30dd33f70d560c2356c6cadbacc4067740eaa2edec5b543cee9af6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_94ff9ac9ab77a239949e7eb13a739da71d906e9406ed836f3b4af3911bbe7360 = $this->env->getExtension("native_profiler");
        $__internal_94ff9ac9ab77a239949e7eb13a739da71d906e9406ed836f3b4af3911bbe7360->enter($__internal_94ff9ac9ab77a239949e7eb13a739da71d906e9406ed836f3b4af3911bbe7360_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_94ff9ac9ab77a239949e7eb13a739da71d906e9406ed836f3b4af3911bbe7360->leave($__internal_94ff9ac9ab77a239949e7eb13a739da71d906e9406ed836f3b4af3911bbe7360_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
