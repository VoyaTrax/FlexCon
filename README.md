FlexCon
=======

A Symfony project created on February 26, 2016, 10:01 pm.

**FlexCon** starts as a very nice, yet basic, blogging system. We will add:

  * Admin Interface
  * User Login Method
  * Cookie Warning & Consent
  * Some sort of _Updater_
  * An _extension/plugin system_
  * Maybe a _Sitemap_.
  * A simple to use _CAPTCHA_  ( _RECAPTCHA_ ??? )
  
This would then be our _CORE SYSTEM_. Including the _EXTENSION SYSTEM_ here 
gives us the advantage of extending the _CORE_ to include:

  * MODULES. such as
    * Forums
    * Downloads
    * Image Gallery
    * Wiki
    * whatever would be needed, can be created.
    
  * THEMES. 
  
  * PLUGINS ???
    * Extending Core Functions ???
    * Extending Modules ???
    
    
    
License
=======
    
The MIT License (MIT)

Copyright (c) 2016 JMColeman <hyperclock@jmcoleman.eu>

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.